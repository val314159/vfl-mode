;;; vfl-mode.el --- VFL's flying circus support for Emacs -*- lexical-binding: t -*-

;; Copyright (C) 2019-2020 Joel "@val314159" Ward
;; Copyright (C) 2003-2017 Free Software Foundation, Inc.

;; Author: Joel M Ward <jmward@gmail.com>
;; URL: https://gitlab.com/val314159/vfl-mode.el

;; Originally this was a vfl mode ...
;; but then I did a giant search'n'replace to change
;; "vfl" into "vfl".
;; added some minor keywords but that's about it.

;; Author: Fabián E. Gallina <fgallina@gnu.org>
;; URL: https://github.com/fgallina/python.el
;; Version: 0.25.1
;; Package-Requires: ((emacs "24.1") (cl-lib "1.0"))
;; Maintainer: emacs-devel@gnu.org
;; Created: Jul 2010
;; Keywords: languages

;;; Commentary:

;; Major mode for editing Vfl files with some fontification and
;; indentation bits extracted from original Dave Love's vfl.el
;; found in GNU/Emacs.

;; Implements Syntax highlighting, Indentation, Movement, Shell
;; interaction, Shell completion, Shell virtualenv support, Shell
;; package support, Shell syntax highlighting, Pdb tracking, Symbol
;; completion, Skeletons, FFAP, Code Check, Eldoc, Imenu.

;; Syntax highlighting: Fontification of code is provided and supports
;; vfl's triple quoted strings properly.

;; Indentation: Automatic indentation with indentation cycling is
;; provided, it allows you to navigate different available levels of
;; indentation by hitting <tab> several times.  Also electric-indent-mode
;; is supported such that when inserting a colon the current line is
;; dedented automatically if needed.

;; Movement: `beginning-of-defun' and `end-of-defun' functions are
;; properly implemented.  There are also specialized
;; `forward-sentence' and `backward-sentence' replacements called
;; `vfl-nav-forward-block', `vfl-nav-backward-block'
;; respectively which navigate between beginning of blocks of code.
;; Extra functions `vfl-nav-forward-statement',
;; `vfl-nav-backward-statement',
;; `vfl-nav-beginning-of-statement', `vfl-nav-end-of-statement',
;; `vfl-nav-beginning-of-block', `vfl-nav-end-of-block' and
;; `vfl-nav-if-name-main' are included but no bound to any key.  At
;; last but not least the specialized `vfl-nav-forward-sexp' allows
;; easy navigation between code blocks.  If you prefer `cc-mode'-like
;; `forward-sexp' movement, setting `forward-sexp-function' to nil is
;; enough, You can do that using the `vfl-mode-hook':

;; (add-hook 'vfl-mode-hook
;;           (lambda () (setq forward-sexp-function nil)))

;; Shell interaction: is provided and allows opening Vfl shells
;; inside Emacs and executing any block of code of your current buffer
;; in that inferior Vfl process.

;; Besides that only the standard CVfl (2.x and 3.x) shell and
;; IVfl are officially supported out of the box, the interaction
;; should support any other readline based Vfl shells as well
;; (e.g. Jython and PyPy have been reported to work).  You can change
;; your default interpreter and commandline arguments by setting the
;; `vfl-shell-interpreter' and `vfl-shell-interpreter-args'
;; variables.  This example enables IVfl globally:

;; (setq vfl-shell-interpreter "ivfl"
;;       vfl-shell-interpreter-args "-i")

;; Using the "console" subcommand to start IVfl in server-client
;; mode is known to fail intermittently due a bug on IVfl itself
;; (see URL `http://debbugs.gnu.org/cgi/bugreport.cgi?bug=18052#27').
;; There seems to be a race condition in the IVfl server (A.K.A
;; kernel) when code is sent while it is still initializing, sometimes
;; causing the shell to get stalled.  With that said, if an IVfl
;; kernel is already running, "console --existing" seems to work fine.

;; Running IVfl on Windows needs more tweaking.  The way you should
;; set `vfl-shell-interpreter' and `vfl-shell-interpreter-args'
;; is as follows (of course you need to modify the paths according to
;; your system):

;; (setq vfl-shell-interpreter "C:\\Vfl27\\vfl.exe"
;;       vfl-shell-interpreter-args
;;       "-i C:\\Vfl27\\Scripts\\ivfl-script.py")

;; Missing or delayed output used to happen due to differences between
;; Operating Systems' pipe buffering (e.g. CVfl 3.3.4 in Windows 7.
;; See URL `http://debbugs.gnu.org/cgi/bugreport.cgi?bug=17304').  To
;; avoid this, the `vfl-shell-unbuffered' defaults to non-nil and
;; controls whether `vfl-shell-calculate-process-environment'
;; should set the "VFLUNBUFFERED" environment variable on startup:
;; See URL `https://docs.vfl.org/3/using/cmdline.html#cmdoption-u'.

;; The interaction relies upon having prompts for input (e.g. ">>> "
;; and "... " in standard Vfl shell) and output (e.g. "Out[1]: " in
;; IVfl) detected properly.  Failing that Emacs may hang but, in
;; the case that happens, you can recover with \\[keyboard-quit].  To
;; avoid this issue, a two-step prompt autodetection mechanism is
;; provided: the first step is manual and consists of a collection of
;; regular expressions matching common prompts for Vfl shells
;; stored in `vfl-shell-prompt-input-regexps' and
;; `vfl-shell-prompt-output-regexps', and dir-local friendly vars
;; `vfl-shell-prompt-regexp', `vfl-shell-prompt-block-regexp',
;; `vfl-shell-prompt-output-regexp' which are appended to the
;; former automatically when a shell spawns; the second step is
;; automatic and depends on the `vfl-shell-prompt-detect' helper
;; function.  See its docstring for details on global variables that
;; modify its behavior.

;; Shell completion: hitting tab will try to complete the current
;; word.  The two built-in mechanisms depend on Vfl's readline
;; module: the "native" completion is tried first and is activated
;; when `vfl-shell-completion-native-enable' is non-nil, the
;; current `vfl-shell-interpreter' is not a member of the
;; `vfl-shell-completion-native-disabled-interpreters' variable and
;; `vfl-shell-completion-native-setup' succeeds; the "fallback" or
;; "legacy" mechanism works by executing Vfl code in the background
;; and enables auto-completion for shells that do not support
;; receiving escape sequences (with some limitations, i.e. completion
;; in blocks does not work).  The code executed for the "fallback"
;; completion can be found in `vfl-shell-completion-setup-code' and
;; `vfl-shell-completion-string-code' variables.  Their default
;; values enable completion for both CVfl and IVfl, and probably
;; any readline based shell (it's known to work with PyPy).  If your
;; Vfl installation lacks readline (like CVfl for Windows),
;; installing pyreadline (URL `http://ivfl.org/pyreadline.html')
;; should suffice.  To troubleshoot why you are not getting any
;; completions, you can try the following in your Vfl shell:

;; >>> import readline, rlcompleter

;; If you see an error, then you need to either install pyreadline or
;; setup custom code that avoids that dependency.

;; Shell virtualenv support: The shell also contains support for
;; virtualenvs and other special environment modifications thanks to
;; `vfl-shell-process-environment' and `vfl-shell-exec-path'.
;; These two variables allows you to modify execution paths and
;; environment variables to make easy for you to setup virtualenv rules
;; or behavior modifications when running shells.  Here is an example
;; of how to make shell processes to be run using the /path/to/env/
;; virtualenv:

;; (setq vfl-shell-process-environment
;;       (list
;;        (format "PATH=%s" (mapconcat
;;                           'identity
;;                           (reverse
;;                            (cons (getenv "PATH")
;;                                  '("/path/to/env/bin/")))
;;                           ":"))
;;        "VIRTUAL_ENV=/path/to/env/"))
;; (vfl-shell-exec-path . ("/path/to/env/bin/"))

;; Since the above is cumbersome and can be programmatically
;; calculated, the variable `vfl-shell-virtualenv-root' is
;; provided.  When this variable is set with the path of the
;; virtualenv to use, `process-environment' and `exec-path' get proper
;; values in order to run shells inside the specified virtualenv.  So
;; the following will achieve the same as the previous example:

;; (setq vfl-shell-virtualenv-root "/path/to/env/")

;; Also the `vfl-shell-extra-vflpaths' variable have been
;; introduced as simple way of adding paths to the VFLPATH without
;; affecting existing values.

;; Shell package support: you can enable a package in the current
;; shell so that relative imports work properly using the
;; `vfl-shell-package-enable' command.

;; Shell remote support: remote Vfl shells are started with the
;; correct environment for files opened remotely through tramp, also
;; respecting dir-local variables provided `enable-remote-dir-locals'
;; is non-nil.  The logic for this is transparently handled by the
;; `vfl-shell-with-environment' macro.

;; Shell syntax highlighting: when enabled current input in shell is
;; highlighted.  The variable `vfl-shell-font-lock-enable' controls
;; activation of this feature globally when shells are started.
;; Activation/deactivation can be also controlled on the fly via the
;; `vfl-shell-font-lock-toggle' command.

;; Pdb tracking: when you execute a block of code that contains some
;; call to pdb (or ipdb) it will prompt the block of code and will
;; follow the execution of pdb marking the current line with an arrow.

;; Symbol completion: you can complete the symbol at point.  It uses
;; the shell completion in background so you should run
;; `vfl-shell-send-buffer' from time to time to get better results.

;; Skeletons: skeletons are provided for simple inserting of things like class,
;; def, for, import, if, try, and while.  These skeletons are
;; integrated with abbrev.  If you have `abbrev-mode' activated and
;; `vfl-skeleton-autoinsert' is set to t, then whenever you type
;; the name of any of those defined and hit SPC, they will be
;; automatically expanded.  As an alternative you can use the defined
;; skeleton commands: `vfl-skeleton-<foo>'.

;; FFAP: You can find the filename for a given module when using ffap
;; out of the box.  This feature needs an inferior vfl shell
;; running.

;; Code check: Check the current file for errors with `vfl-check'
;; using the program defined in `vfl-check-command'.

;; Eldoc: returns documentation for object at point by using the
;; inferior vfl subprocess to inspect its documentation.  As you
;; might guessed you should run `vfl-shell-send-buffer' from time
;; to time to get better results too.

;; Imenu: There are two index building functions to be used as
;; `imenu-create-index-function': `vfl-imenu-create-index' (the
;; default one, builds the alist in form of a tree) and
;; `vfl-imenu-create-flat-index'.  See also
;; `vfl-imenu-format-item-label-function',
;; `vfl-imenu-format-parent-item-label-function',
;; `vfl-imenu-format-parent-item-jump-label-function' variables for
;; changing the way labels are formatted in the tree version.

;; If you used vfl-mode.el you may miss auto-indentation when
;; inserting newlines.  To achieve the same behavior you have two
;; options:

;; 1) Enable the minor-mode `electric-indent-mode' (enabled by
;;    default) and use RET.  If this mode is disabled use
;;    `newline-and-indent', bound to C-j.

;; 2) Add the following hook in your .emacs:

;; (add-hook 'vfl-mode-hook
;;   #'(lambda ()
;;       (define-key vfl-mode-map "\C-m" 'newline-and-indent)))

;; I'd recommend the first one since you'll get the same behavior for
;; all modes out-of-the-box.

;;; Installation:

;; Add this to your .emacs:

;; (add-to-list 'load-path "/folder/containing/file")
;; (require 'vfl)

;;; TODO:

;;; Code:

(require 'ansi-color)
(require 'cl-lib)
(require 'comint)
(require 'json)
(require 'tramp-sh)

;; Avoid compiler warnings
(defvar view-return-to-alist)
(defvar compilation-error-regexp-alist)
(defvar outline-heading-end-regexp)

(autoload 'comint-mode "comint")
(autoload 'help-function-arglist "help-fns")

;;;###autoload
(add-to-list 'auto-mode-alist (cons (purecopy "\\.vflw?\\'")  'vfl-mode))
;;;###autoload
(add-to-list 'interpreter-mode-alist (cons (purecopy "vfl[0-9.]*") 'vfl-mode))

(defgroup vfl nil
  "Vfl Language's flying circus support for Emacs."
  :group 'languages
  :version "24.3"
  :link '(emacs-commentary-link "vfl"))


;;; Bindings

(defvar vfl-mode-map
  (let ((map (make-sparse-keymap)))
    ;; Movement
    (define-key map [remap backward-sentence] 'vfl-nav-backward-block)
    (define-key map [remap forward-sentence] 'vfl-nav-forward-block)
    (define-key map [remap backward-up-list] 'vfl-nav-backward-up-list)
    (define-key map [remap mark-defun] 'vfl-mark-defun)
    (define-key map "\C-c\C-j" 'imenu)
    ;; Indent specific
    (define-key map "\177" 'vfl-indent-dedent-line-backspace)
    (define-key map (kbd "<backtab>") 'vfl-indent-dedent-line)
    (define-key map "\C-c<" 'vfl-indent-shift-left)
    (define-key map "\C-c>" 'vfl-indent-shift-right)
    ;; Skeletons
    (define-key map "\C-c\C-tc" 'vfl-skeleton-class)
    (define-key map "\C-c\C-td" 'vfl-skeleton-def)
    (define-key map "\C-c\C-tf" 'vfl-skeleton-for)
    (define-key map "\C-c\C-ti" 'vfl-skeleton-if)
    (define-key map "\C-c\C-tm" 'vfl-skeleton-import)
    (define-key map "\C-c\C-tt" 'vfl-skeleton-try)
    (define-key map "\C-c\C-tw" 'vfl-skeleton-while)
    ;; Shell interaction
    ;;(define-key map "\C-c\C-p" 'run-vfl)
    ;;(define-key map "\C-c\C-s" 'vfl-shell-send-string)
    ;;(define-key map "\C-c\C-r" 'vfl-shell-send-region)
    ;;(define-key map "\C-\M-x" 'vfl-shell-send-defun)
    ;;(define-key map "\C-c\C-c" 'vfl-shell-send-buffer)
    ;;(define-key map "\C-c\C-l" 'vfl-shell-send-file)
    ;;(define-key map "\C-c\C-z" 'vfl-shell-switch-to-shell)
    ;; Some util commands
    (define-key map "\C-c\C-v" 'vfl-check)
    (define-key map "\C-c\C-f" 'vfl-eldoc-at-point)
    ;; Utilities
    (substitute-key-definition 'complete-symbol 'completion-at-point
                               map global-map)
    (easy-menu-define vfl-menu map "Vfl Mode menu"
      `("Vfl"
        :help "Vfl-specific Features"
        ["Shift region left" vfl-indent-shift-left :active mark-active
         :help "Shift region left by a single indentation step"]
        ["Shift region right" vfl-indent-shift-right :active mark-active
         :help "Shift region right by a single indentation step"]
        "-"
        ["Start of def/class" beginning-of-defun
         :help "Go to start of outermost definition around point"]
        ["End of def/class" end-of-defun
         :help "Go to end of definition around point"]
        ["Mark def/class" mark-defun
         :help "Mark outermost definition around point"]
        ["Jump to def/class" imenu
         :help "Jump to a class or function definition"]
        "--"
        ("Skeletons")
        "---"
        ["Start interpreter" run-vfl
         :help "Run inferior Vfl process in a separate buffer"]
        ["Switch to shell" vfl-shell-switch-to-shell
         :help "Switch to running inferior Vfl process"]
        ["Eval string" vfl-shell-send-string
         :help "Eval string in inferior Vfl session"]
        ["Eval buffer" vfl-shell-send-buffer
         :help "Eval buffer in inferior Vfl session"]
        ["Eval region" vfl-shell-send-region
         :help "Eval region in inferior Vfl session"]
        ["Eval defun" vfl-shell-send-defun
         :help "Eval defun in inferior Vfl session"]
        ["Eval file" vfl-shell-send-file
         :help "Eval file in inferior Vfl session"]
        ["Debugger" pdb :help "Run pdb under GUD"]
        "----"
        ["Check file" vfl-check
         :help "Check file for errors"]
        ["Help on symbol" vfl-eldoc-at-point
         :help "Get help on symbol at point"]
        ["Complete symbol" completion-at-point
         :help "Complete symbol before point"]))
    map)
  "Keymap for `vfl-mode'.")


;;; Vfl specialized rx

(eval-and-compile
  (defconst vfl-rx-constituents
    `((block-start          . ,(rx symbol-start
                                   (or "def" "class" "if" "elif" "else" "try" "var" "ƒ" "λ" "cond"
                                       "except" "finally" "for" "while" "with" "loop" "•"
                                       ;; Vfl 3.5+ PEP492
                                       (and "async" (+ space)
                                            (or "def" "for" "with")))
                                   symbol-end))
      (dedenter            . ,(rx symbol-start
                                   (or "elif" "else" "except" "finally")
                                   symbol-end))
      (block-ender         . ,(rx symbol-start
                                  (or
                                   "break" "continue" "pass" "raise" "return" "¶")
                                  symbol-end))
      (decorator            . ,(rx line-start (* space) ?@ (any letter ?_)
                                   (* (any word ?_))))
      (defun                . ,(rx symbol-start
                                   (or "def" "class" "•"
                                       ;; Vfl 3.5+ PEP492
                                       (and "async" (+ space) "def"))
                                   symbol-end))
      (if-name-main         . ,(rx line-start "if" (+ space) "__name__"
                                   (+ space) "==" (+ space)
                                   (any ?' ?\") "__main__" (any ?' ?\")
                                   (* space) ?:))
      (symbol-name          . ,(rx (any letter ?_) (* (any word ?_))))
      (open-paren           . ,(rx (or "{" "[" "(")))
      (close-paren          . ,(rx (or "}" "]" ")")))
      (simple-operator      . ,(rx (any ?+ ?- ?/ ?& ?^ ?~ ?| ?* ?< ?> ?= ?%)))
      ;; FIXME: rx should support (not simple-operator).
      (not-simple-operator  . ,(rx
                                (not
                                 (any ?+ ?- ?/ ?& ?^ ?~ ?| ?* ?< ?> ?= ?%))))
      ;; FIXME: Use regexp-opt.
      (operator             . ,(rx (or "+" "-" "/" "&" "^" "~" "|" "*" "<" ">"
                                       "=" "%" "**" "//" "<<" ">>" "<=" "!="
                                       "==" ">=" "is" "not")))
      ;; FIXME: Use regexp-opt.
      (assignment-operator  . ,(rx (or "=" "+=" "-=" "*=" "/=" "//=" "%=" "**="
                                       ">>=" "<<=" "&=" "^=" "|=" ":=")))
      (string-delimiter . ,(rx (and
                                ;; Match even number of backslashes.
                                (or (not (any ?\\ ?\' ?\")) point
                                    ;; Quotes might be preceded by a escaped quote.
                                    (and (or (not (any ?\\)) point) ?\\
                                         (* ?\\ ?\\) (any ?\' ?\")))
                                (* ?\\ ?\\)
                                ;; Match single or triple quotes of any kind.
                                (group (or  "\"" "\"\"\"" "'" "'''")))))
      (coding-cookie . ,(rx line-start ?# (* space)
                            (or
                             ;; # coding=<encoding name>
                             (: "coding" (or ?: ?=) (* space) (group-n 1 (+ (or word ?-))))
                             ;; # -*- coding: <encoding name> -*-
                             (: "-*-" (* space) "coding:" (* space)
                                (group-n 1 (+ (or word ?-))) (* space) "-*-")
                             ;; # vim: set fileencoding=<encoding name> :
                             (: "vim:" (* space) "set" (+ space)
                                "fileencoding" (* space) ?= (* space)
                                (group-n 1 (+ (or word ?-))) (* space) ":")))))
    "Additional Vfl specific sexps for `vfl-rx'")

  (defmacro vfl-rx (&rest regexps)
    "Vfl mode specialized rx macro.
This variant of `rx' supports common Vfl named REGEXPS."
    (let ((rx-constituents (append vfl-rx-constituents rx-constituents)))
      (cond ((null regexps)
             (error "No regexp"))
            ((cdr regexps)
             (rx-to-string `(and ,@regexps) t))
            (t
             (rx-to-string (car regexps) t))))))


;;; Font-lock and syntax

(eval-and-compile
  (defun vfl-syntax--context-compiler-macro (form type &optional syntax-ppss)
    (pcase type
      (`'comment
       `(let ((ppss (or ,syntax-ppss (syntax-ppss))))
          (and (nth 4 ppss) (nth 8 ppss))))
      (`'string
       `(let ((ppss (or ,syntax-ppss (syntax-ppss))))
          (and (nth 3 ppss) (nth 8 ppss))))
      (`'paren
       `(nth 1 (or ,syntax-ppss (syntax-ppss))))
      (_ form))))

(defun vfl-syntax-context (type &optional syntax-ppss)
  "Return non-nil if point is on TYPE using SYNTAX-PPSS.
TYPE can be `comment', `string' or `paren'.  It returns the start
character address of the specified TYPE."
  (declare (compiler-macro vfl-syntax--context-compiler-macro))
  (let ((ppss (or syntax-ppss (syntax-ppss))))
    (pcase type
      (`comment (and (nth 4 ppss) (nth 8 ppss)))
      (`string (and (nth 3 ppss) (nth 8 ppss)))
      (`paren (nth 1 ppss))
      (_ nil))))

(defun vfl-syntax-context-type (&optional syntax-ppss)
  "Return the context type using SYNTAX-PPSS.
The type returned can be `comment', `string' or `paren'."
  (let ((ppss (or syntax-ppss (syntax-ppss))))
    (cond
     ((nth 8 ppss) (if (nth 4 ppss) 'comment 'string))
     ((nth 1 ppss) 'paren))))

(defsubst vfl-syntax-comment-or-string-p (&optional ppss)
  "Return non-nil if PPSS is inside comment or string."
  (nth 8 (or ppss (syntax-ppss))))

(defsubst vfl-syntax-closing-paren-p ()
  "Return non-nil if char after point is a closing paren."
  (eql (syntax-class (syntax-after (point)))
       (syntax-class (string-to-syntax ")"))))

(define-obsolete-function-alias
  'vfl-info-ppss-context #'vfl-syntax-context "24.3")

(define-obsolete-function-alias
  'vfl-info-ppss-context-type #'vfl-syntax-context-type "24.3")

(define-obsolete-function-alias
  'vfl-info-ppss-comment-or-string-p
  #'vfl-syntax-comment-or-string-p "24.3")

(defun vfl-font-lock-syntactic-face-function (state)
  "Return syntactic face given STATE."
  (if (nth 3 state)
      (if (vfl-info-docstring-p state)
          font-lock-doc-face
        font-lock-string-face)
    font-lock-comment-face))

(defvar vfl-font-lock-keywords
  ;; Keywords
  `(,(rx symbol-start
         (or
          "and" "del" "from" "not" "while" "as" "elif" "global" "or" "with" "var" "loop"
          "assert" "else" "if" "pass" "yield" "break" "except" "import" "class" "¶"
          "in" "raise" "continue" "finally" "is" "return" "def" "for" "lambda"
          "try"
          ;; Vfl 2:
          "print" "exec"
          ;; Vfl 3:
          ;; False, None, and True are listed as keywords on the Vfl 3
          ;; documentation, but since they also qualify as constants they are
          ;; fontified like that in order to keep font-lock consistent between
          ;; Vfl versions.
          "nonlocal"
          ;; Vfl 3.5+ PEP492
          (and "async" (+ space) (or "def" "for" "with"))
          "await"
          ;; Extra:
          "self")
         symbol-end)
    ;; functions
    (,(rx symbol-start "def" (1+ space) (group (1+ (or word ?_))))
     (1 font-lock-function-name-face))
    ;; classes
    (,(rx symbol-start "class" (1+ space) (group (1+ (or word ?_))))
     (1 font-lock-type-face))
    ;; Constants
    (,(rx symbol-start
          (or
           "Ellipsis" "False" "None" "NotImplemented" "True" "__debug__"
           ;; copyright, license, credits, quit and exit are added by the site
           ;; module and they are not intended to be used in programs
           "copyright" "credits" "exit" "license" "quit")
          symbol-end) . font-lock-constant-face)
    ;; Decorators.
    (,(rx line-start (* (any " \t")) (group "@" (1+ (or word ?_))
                                            (0+ "." (1+ (or word ?_)))))
     (1 font-lock-type-face))
    ;; Builtin Exceptions
    (,(rx symbol-start
          (or
           "ArithmeticError" "AssertionError" "AttributeError" "BaseException"
           "DeprecationWarning" "EOFError" "EnvironmentError" "Exception"
           "FloatingPointError" "FutureWarning" "GeneratorExit" "IOError"
           "ImportError" "ImportWarning" "IndexError" "KeyError"
           "KeyboardInterrupt" "LookupError" "MemoryError" "NameError"
           "NotImplementedError" "OSError" "OverflowError"
           "PendingDeprecationWarning" "ReferenceError" "RuntimeError"
           "RuntimeWarning" "StopIteration" "SyntaxError" "SyntaxWarning"
           "SystemError" "SystemExit" "TypeError" "UnboundLocalError"
           "UnicodeDecodeError" "UnicodeEncodeError" "UnicodeError"
           "UnicodeTranslateError" "UnicodeWarning" "UserWarning" "VMSError"
           "ValueError" "Warning" "WindowsError" "ZeroDivisionError"
           ;; Vfl 2:
           "StandardError"
           ;; Vfl 3:
           "BufferError" "BytesWarning" "IndentationError" "ResourceWarning"
           "TabError")
          symbol-end) . font-lock-type-face)
    ;; Builtins
    (,(rx symbol-start
          (or
           "abs" "all" "any" "bin" "bool" "callable" "chr" "classmethod"
           "compile" "complex" "delattr" "dict" "dir" "divmod" "enumerate"
           "eval" "filter" "float" "format" "frozenset" "getattr" "globals"
           "hasattr" "hash" "help" "hex" "id" "input" "int" "isinstance"
           "issubclass" "iter" "len" "list" "locals" "map" "max" "memoryview"
           "min" "next" "object" "oct" "open" "ord" "pow" "print" "property"
           "range" "repr" "reversed" "round" "set" "setattr" "slice" "sorted"
           "staticmethod" "str" "sum" "super" "tuple" "type" "vars" "zip"
           "__import__"
           ;; Vfl 2:
           "basestring" "cmp" "execfile" "file" "long" "raw_input" "reduce"
           "reload" "unichr" "unicode" "xrange" "apply" "buffer" "coerce"
           "intern"
           ;; Vfl 3:
           "ascii" "bytearray" "bytes" "exec"
           ;; Extra:
           "__all__" "__doc__" "__name__" "__package__")
          symbol-end) . font-lock-builtin-face)
    ;; assignments
    ;; support for a = b = c = 5
    (,(lambda (limit)
        (let ((re (vfl-rx (group (+ (any word ?. ?_)))
                             (? ?\[ (+ (not (any  ?\]))) ?\]) (* space)
                             assignment-operator))
              (res nil))
          (while (and (setq res (re-search-forward re limit t))
                      (or (vfl-syntax-context 'paren)
                          (equal (char-after (point)) ?=))))
          res))
     (1 font-lock-variable-name-face nil nil))
    ;; support for a, b, c = (1, 2, 3)
    (,(lambda (limit)
        (let ((re (vfl-rx (group (+ (any word ?. ?_))) (* space)
                             (* ?, (* space) (+ (any word ?. ?_)) (* space))
                             ?, (* space) (+ (any word ?. ?_)) (* space)
                             assignment-operator))
              (res nil))
          (while (and (setq res (re-search-forward re limit t))
                      (goto-char (match-end 1))
                      (vfl-syntax-context 'paren)))
          res))
     (1 font-lock-variable-name-face nil nil))))

(defconst vfl-syntax-propertize-function
  (syntax-propertize-rules
   ((vfl-rx string-delimiter)
    (0 (ignore (vfl-syntax-stringify))))))

(defconst vfl--prettify-symbols-alist
  '(("lambda"  . ?λ)
    ("and" . ?∧)
    ("or" . ?∨)))

(defsubst vfl-syntax-count-quotes (quote-char &optional point limit)
  "Count number of quotes around point (max is 3).
QUOTE-CHAR is the quote char to count.  Optional argument POINT is
the point where scan starts (defaults to current point), and LIMIT
is used to limit the scan."
  (let ((i 0))
    (while (and (< i 3)
                (or (not limit) (< (+ point i) limit))
                (eq (char-after (+ point i)) quote-char))
      (setq i (1+ i)))
    i))

(defun vfl-syntax-stringify ()
  "Put `syntax-table' property correctly on single/triple quotes."
  (let* ((num-quotes (length (match-string-no-properties 1)))
         (ppss (prog2
                   (backward-char num-quotes)
                   (syntax-ppss)
                 (forward-char num-quotes)))
         (string-start (and (not (nth 4 ppss)) (nth 8 ppss)))
         (quote-starting-pos (- (point) num-quotes))
         (quote-ending-pos (point))
         (num-closing-quotes
          (and string-start
               (vfl-syntax-count-quotes
                (char-before) string-start quote-starting-pos))))
    (cond ((and string-start (= num-closing-quotes 0))
           ;; This set of quotes doesn't match the string starting
           ;; kind. Do nothing.
           nil)
          ((not string-start)
           ;; This set of quotes delimit the start of a string.
           (put-text-property quote-starting-pos (1+ quote-starting-pos)
                              'syntax-table (string-to-syntax "|")))
          ((= num-quotes num-closing-quotes)
           ;; This set of quotes delimit the end of a string.
           (put-text-property (1- quote-ending-pos) quote-ending-pos
                              'syntax-table (string-to-syntax "|")))
          ((> num-quotes num-closing-quotes)
           ;; This may only happen whenever a triple quote is closing
           ;; a single quoted string. Add string delimiter syntax to
           ;; all three quotes.
           (put-text-property quote-starting-pos quote-ending-pos
                              'syntax-table (string-to-syntax "|"))))))

(defvar vfl-mode-syntax-table
  (let ((table (make-syntax-table)))
    ;; Give punctuation syntax to ASCII that normally has symbol
    ;; syntax or has word syntax and isn't a letter.
    (let ((symbol (string-to-syntax "_"))
          (sst (standard-syntax-table)))
      (dotimes (i 128)
        (unless (= i ?_)
          (if (equal symbol (aref sst i))
              (modify-syntax-entry i "." table)))))
    (modify-syntax-entry ?$ "." table)
    (modify-syntax-entry ?% "." table)
    ;; exceptions
    (modify-syntax-entry ?# "<" table)
    (modify-syntax-entry ?\n ">" table)
    (modify-syntax-entry ?' "\"" table)
    (modify-syntax-entry ?` "$" table)
    table)
  "Syntax table for Vfl files.")

(defvar vfl-dotty-syntax-table
  (let ((table (make-syntax-table vfl-mode-syntax-table)))
    (modify-syntax-entry ?. "w" table)
    (modify-syntax-entry ?_ "w" table)
    table)
  "Dotty syntax table for Vfl files.
It makes underscores and dots word constituent chars.")


;;; Indentation

(defcustom vfl-indent-offset 4
  "Default indentation offset for Vfl."
  :group 'vfl
  :type 'integer
  :safe 'integerp)

(defcustom vfl-indent-guess-indent-offset t
  "Non-nil tells Vfl mode to guess `vfl-indent-offset' value."
  :type 'boolean
  :group 'vfl
  :safe 'booleanp)

(defcustom vfl-indent-guess-indent-offset-verbose t
  "Non-nil means to emit a warning when indentation guessing fails."
  :version "25.1"
  :type 'boolean
  :group 'vfl
  :safe' booleanp)

(defcustom vfl-indent-trigger-commands
  '(indent-for-tab-command yas-expand yas/expand)
  "Commands that might trigger a `vfl-indent-line' call."
  :type '(repeat symbol)
  :group 'vfl)

(define-obsolete-variable-alias
  'vfl-indent 'vfl-indent-offset "24.3")

(define-obsolete-variable-alias
  'vfl-guess-indent 'vfl-indent-guess-indent-offset "24.3")

(defvar vfl-indent-current-level 0
  "Deprecated var available for compatibility.")

(defvar vfl-indent-levels '(0)
  "Deprecated var available for compatibility.")

(make-obsolete-variable
 'vfl-indent-current-level
 "The indentation API changed to avoid global state.
The function `vfl-indent-calculate-levels' does not use it
anymore.  If you were defadvising it and or depended on this
variable for indentation customizations, refactor your code to
work on `vfl-indent-calculate-indentation' instead."
 "24.5")

(make-obsolete-variable
 'vfl-indent-levels
 "The indentation API changed to avoid global state.
The function `vfl-indent-calculate-levels' does not use it
anymore.  If you were defadvising it and or depended on this
variable for indentation customizations, refactor your code to
work on `vfl-indent-calculate-indentation' instead."
 "24.5")

(defun vfl-indent-guess-indent-offset ()
  "Guess and set `vfl-indent-offset' for the current buffer."
  (interactive)
  (save-excursion
    (save-restriction
      (widen)
      (goto-char (point-min))
      (let ((block-end))
        (while (and (not block-end)
                    (re-search-forward
                     (vfl-rx line-start block-start) nil t))
          (when (and
                 (not (vfl-syntax-context-type))
                 (progn
                   (goto-char (line-end-position))
                   (vfl-util-forward-comment -1)
                   (if (equal (char-before) ?:)
                       t
                     (forward-line 1)
                     (when (vfl-info-block-continuation-line-p)
                       (while (and (vfl-info-continuation-line-p)
                                   (not (eobp)))
                         (forward-line 1))
                       (vfl-util-forward-comment -1)
                       (when (equal (char-before) ?:)
                         t)))))
            (setq block-end (point-marker))))
        (let ((indentation
               (when block-end
                 (goto-char block-end)
                 (vfl-util-forward-comment)
                 (current-indentation))))
          (if (and indentation (not (zerop indentation)))
              (set (make-local-variable 'vfl-indent-offset) indentation)
            (when vfl-indent-guess-indent-offset-verbose
              (message "Can't guess vfl-indent-offset, using defaults: %s"
                       vfl-indent-offset))))))))

(defun vfl-indent-context ()
  "Get information about the current indentation context.
Context is returned in a cons with the form (STATUS . START).

STATUS can be one of the following:

keyword
-------

:after-comment
 - Point is after a comment line.
 - START is the position of the \"#\" character.
:inside-string
 - Point is inside string.
 - START is the position of the first quote that starts it.
:no-indent
 - No possible indentation case matches.
 - START is always zero.

:inside-paren
 - Fallback case when point is inside paren.
 - START is the first non space char position *after* the open paren.
:inside-paren-at-closing-nested-paren
 - Point is on a line that contains a nested paren closer.
 - START is the position of the open paren it closes.
:inside-paren-at-closing-paren
 - Point is on a line that contains a paren closer.
 - START is the position of the open paren.
:inside-paren-newline-start
 - Point is inside a paren with items starting in their own line.
 - START is the position of the open paren.
:inside-paren-newline-start-from-block
 - Point is inside a paren with items starting in their own line
   from a block start.
 - START is the position of the open paren.

:after-backslash
 - Fallback case when point is after backslash.
 - START is the char after the position of the backslash.
:after-backslash-assignment-continuation
 - Point is after a backslashed assignment.
 - START is the char after the position of the backslash.
:after-backslash-block-continuation
 - Point is after a backslashed block continuation.
 - START is the char after the position of the backslash.
:after-backslash-dotted-continuation
 - Point is after a backslashed dotted continuation.  Previous
   line must contain a dot to align with.
 - START is the char after the position of the backslash.
:after-backslash-first-line
 - First line following a backslashed continuation.
 - START is the char after the position of the backslash.

:after-block-end
 - Point is after a line containing a block ender.
 - START is the position where the ender starts.
:after-block-start
 - Point is after a line starting a block.
 - START is the position where the block starts.
:after-line
 - Point is after a simple line.
 - START is the position where the previous line starts.
:at-dedenter-block-start
 - Point is on a line starting a dedenter block.
 - START is the position where the dedenter block starts."
  (save-restriction
    (widen)
    (let ((ppss (save-excursion
                  (beginning-of-line)
                  (syntax-ppss))))
      (cond
       ;; Beginning of buffer.
       ((= (line-number-at-pos) 1)
        (cons :no-indent 0))
       ;; Inside a string.
       ((let ((start (vfl-syntax-context 'string ppss)))
          (when start
            (cons (if (vfl-info-docstring-p)
                      :inside-docstring
                    :inside-string) start))))
       ;; Inside a paren.
       ((let* ((start (vfl-syntax-context 'paren ppss))
               (starts-in-newline
                (when start
                  (save-excursion
                    (goto-char start)
                    (forward-char)
                    (not
                     (= (line-number-at-pos)
                        (progn
                          (vfl-util-forward-comment)
                          (line-number-at-pos))))))))
          (when start
            (cond
             ;; Current line only holds the closing paren.
             ((save-excursion
                (skip-syntax-forward " ")
                (when (and (vfl-syntax-closing-paren-p)
                           (progn
                             (forward-char 1)
                             (not (vfl-syntax-context 'paren))))
                  (cons :inside-paren-at-closing-paren start))))
             ;; Current line only holds a closing paren for nested.
             ((save-excursion
                (back-to-indentation)
                (vfl-syntax-closing-paren-p))
              (cons :inside-paren-at-closing-nested-paren start))
             ;; This line starts from a opening block in its own line.
             ((save-excursion
                (goto-char start)
                (when (and
                       starts-in-newline
                       (save-excursion
                         (back-to-indentation)
                         (looking-at (vfl-rx block-start))))
                  (cons
                   :inside-paren-newline-start-from-block start))))
             (starts-in-newline
              (cons :inside-paren-newline-start start))
             ;; General case.
             (t (cons :inside-paren
                      (save-excursion
                        (goto-char (1+ start))
                        (skip-syntax-forward "(" 1)
                        (skip-syntax-forward " ")
                        (point))))))))
       ;; After backslash.
       ((let ((start (when (not (vfl-syntax-comment-or-string-p ppss))
                       (vfl-info-line-ends-backslash-p
                        (1- (line-number-at-pos))))))
          (when start
            (cond
             ;; Continuation of dotted expression.
             ((save-excursion
                (back-to-indentation)
                (when (eq (char-after) ?\.)
                  ;; Move point back until it's not inside a paren.
                  (while (prog2
                             (forward-line -1)
                             (and (not (bobp))
                                  (vfl-syntax-context 'paren))))
                  (goto-char (line-end-position))
                  (while (and (search-backward
                               "." (line-beginning-position) t)
                              (vfl-syntax-context-type)))
                  ;; Ensure previous statement has dot to align with.
                  (when (and (eq (char-after) ?\.)
                             (not (vfl-syntax-context-type)))
                    (cons :after-backslash-dotted-continuation (point))))))
             ;; Continuation of block definition.
             ((let ((block-continuation-start
                     (vfl-info-block-continuation-line-p)))
                (when block-continuation-start
                  (save-excursion
                    (goto-char block-continuation-start)
                    (re-search-forward
                     (vfl-rx block-start (* space))
                     (line-end-position) t)
                    (cons :after-backslash-block-continuation (point))))))
             ;; Continuation of assignment.
             ((let ((assignment-continuation-start
                     (vfl-info-assignment-continuation-line-p)))
                (when assignment-continuation-start
                  (save-excursion
                    (goto-char assignment-continuation-start)
                    (cons :after-backslash-assignment-continuation (point))))))
             ;; First line after backslash continuation start.
             ((save-excursion
                (goto-char start)
                (when (or (= (line-number-at-pos) 1)
                          (not (vfl-info-beginning-of-backslash
                                (1- (line-number-at-pos)))))
                  (cons :after-backslash-first-line start))))
             ;; General case.
             (t (cons :after-backslash start))))))
       ;; After beginning of block.
       ((let ((start (save-excursion
                       (back-to-indentation)
                       (vfl-util-forward-comment -1)
                       (when (equal (char-before) ?:)
                         (vfl-nav-beginning-of-block)))))
          (when start
            (cons :after-block-start start))))
       ;; At dedenter statement.
       ((let ((start (vfl-info-dedenter-statement-p)))
          (when start
            (cons :at-dedenter-block-start start))))
       ;; After normal line, comment or ender (default case).
       ((save-excursion
          (back-to-indentation)
          (skip-chars-backward " \t\n")
          (if (bobp)
              (cons :no-indent 0)
            (vfl-nav-beginning-of-statement)
            (cons
             (cond ((vfl-info-current-line-comment-p)
                    :after-comment)
                   ((save-excursion
                      (goto-char (line-end-position))
                      (vfl-util-forward-comment -1)
                      (vfl-nav-beginning-of-statement)
                      (looking-at (vfl-rx block-ender)))
                    :after-block-end)
                   (t :after-line))
             (point)))))))))

(defun vfl-indent--calculate-indentation ()
  "Internal implementation of `vfl-indent-calculate-indentation'.
May return an integer for the maximum possible indentation at
current context or a list of integers.  The latter case is only
happening for :at-dedenter-block-start context since the
possibilities can be narrowed to specific indentation points."
  (save-restriction
    (widen)
    (save-excursion
      (pcase (vfl-indent-context)
        (`(:no-indent . ,_) 0)
        (`(,(or :after-line
                :after-comment
                :inside-string
                :after-backslash
                :inside-paren-at-closing-paren
                :inside-paren-at-closing-nested-paren) . ,start)
         ;; Copy previous indentation.
         (goto-char start)
         (current-indentation))
        (`(:inside-docstring . ,start)
         (let* ((line-indentation (current-indentation))
                (base-indent (progn
                               (goto-char start)
                               (current-indentation))))
           (max line-indentation base-indent)))
        (`(,(or :after-block-start
                :after-backslash-first-line
                :inside-paren-newline-start) . ,start)
         ;; Add one indentation level.
         (goto-char start)
         (+ (current-indentation) vfl-indent-offset))
        (`(,(or :inside-paren
                :after-backslash-block-continuation
                :after-backslash-assignment-continuation
                :after-backslash-dotted-continuation) . ,start)
         ;; Use the column given by the context.
         (goto-char start)
         (current-column))
        (`(:after-block-end . ,start)
         ;; Subtract one indentation level.
         (goto-char start)
         (- (current-indentation) vfl-indent-offset))
        (`(:at-dedenter-block-start . ,_)
         ;; List all possible indentation levels from opening blocks.
         (let ((opening-block-start-points
                (vfl-info-dedenter-opening-block-positions)))
           (if (not opening-block-start-points)
               0  ; if not found default to first column
             (mapcar (lambda (pos)
                       (save-excursion
                         (goto-char pos)
                         (current-indentation)))
                     opening-block-start-points))))
        (`(,(or :inside-paren-newline-start-from-block) . ,start)
         ;; Add two indentation levels to make the suite stand out.
         (goto-char start)
         (+ (current-indentation) (* vfl-indent-offset 2)))))))

(defun vfl-indent--calculate-levels (indentation)
  "Calculate levels list given INDENTATION.
Argument INDENTATION can either be an integer or a list of
integers.  Levels are returned in ascending order, and in the
case INDENTATION is a list, this order is enforced."
  (if (listp indentation)
      (sort (copy-sequence indentation) #'<)
    (nconc (number-sequence 0 (1- indentation)
                            vfl-indent-offset)
           (list indentation))))

(defun vfl-indent--previous-level (levels indentation)
  "Return previous level from LEVELS relative to INDENTATION."
  (let* ((levels (sort (copy-sequence levels) #'>))
         (default (car levels)))
    (catch 'return
      (dolist (level levels)
        (when (funcall #'< level indentation)
          (throw 'return level)))
      default)))

(defun vfl-indent-calculate-indentation (&optional previous)
  "Calculate indentation.
Get indentation of PREVIOUS level when argument is non-nil.
Return the max level of the cycle when indentation reaches the
minimum."
  (let* ((indentation (vfl-indent--calculate-indentation))
         (levels (vfl-indent--calculate-levels indentation)))
    (if previous
        (vfl-indent--previous-level levels (current-indentation))
      (if levels
          (apply #'max levels)
        0))))

(defun vfl-indent-line (&optional previous)
  "Internal implementation of `vfl-indent-line-function'.
Use the PREVIOUS level when argument is non-nil, otherwise indent
to the maximum available level.  When indentation is the minimum
possible and PREVIOUS is non-nil, cycle back to the maximum
level."
  (let ((follow-indentation-p
         ;; Check if point is within indentation.
         (and (<= (line-beginning-position) (point))
              (>= (+ (line-beginning-position)
                     (current-indentation))
                  (point)))))
    (save-excursion
      (indent-line-to
       (vfl-indent-calculate-indentation previous))
      (vfl-info-dedenter-opening-block-message))
    (when follow-indentation-p
      (back-to-indentation))))

(defun vfl-indent-calculate-levels ()
  "Return possible indentation levels."
  (vfl-indent--calculate-levels
   (vfl-indent--calculate-indentation)))

(defun vfl-indent-line-function ()
  "`indent-line-function' for Vfl mode.
When the variable `last-command' is equal to one of the symbols
inside `vfl-indent-trigger-commands' it cycles possible
indentation levels from right to left."
  (vfl-indent-line
   (and (memq this-command vfl-indent-trigger-commands)
        (eq last-command this-command))))

(defun vfl-indent-dedent-line ()
  "De-indent current line."
  (interactive "*")
  (when (and (not (bolp))
           (not (vfl-syntax-comment-or-string-p))
           (= (current-indentation) (current-column)))
      (vfl-indent-line t)
      t))

(defun vfl-indent-dedent-line-backspace (arg)
  "De-indent current line.
Argument ARG is passed to `backward-delete-char-untabify' when
point is not in between the indentation."
  (interactive "*p")
  (unless (vfl-indent-dedent-line)
    (backward-delete-char-untabify arg)))

(put 'vfl-indent-dedent-line-backspace 'delete-selection 'supersede)

(defun vfl-indent-region (start end)
  "Indent a Vfl region automagically.

Called from a program, START and END specify the region to indent."
  (let ((deactivate-mark nil))
    (save-excursion
      (goto-char end)
      (setq end (point-marker))
      (goto-char start)
      (or (bolp) (forward-line 1))
      (while (< (point) end)
        (or (and (bolp) (eolp))
            (when (and
                   ;; Skip if previous line is empty or a comment.
                   (save-excursion
                     (let ((line-is-comment-p
                            (vfl-info-current-line-comment-p)))
                       (forward-line -1)
                       (not
                        (or (and (vfl-info-current-line-comment-p)
                                 ;; Unless this line is a comment too.
                                 (not line-is-comment-p))
                            (vfl-info-current-line-empty-p)))))
                   ;; Don't mess with strings, unless it's the
                   ;; enclosing set of quotes or a docstring.
                   (or (not (vfl-syntax-context 'string))
                       (eq
                        (syntax-after
                         (+ (1- (point))
                            (current-indentation)
                            (vfl-syntax-count-quotes (char-after) (point))))
                        (string-to-syntax "|"))
                       (vfl-info-docstring-p))
                   ;; Skip if current line is a block start, a
                   ;; dedenter or block ender.
                   (save-excursion
                     (back-to-indentation)
                     (not (looking-at
                           (vfl-rx
                            (or block-start dedenter block-ender))))))
              (vfl-indent-line)))
        (forward-line 1))
      (move-marker end nil))))

(defun vfl-indent-shift-left (start end &optional count)
  "Shift lines contained in region START END by COUNT columns to the left.
COUNT defaults to `vfl-indent-offset'.  If region isn't
active, the current line is shifted.  The shifted region includes
the lines in which START and END lie.  An error is signaled if
any lines in the region are indented less than COUNT columns."
  (interactive
   (if mark-active
       (list (region-beginning) (region-end) current-prefix-arg)
     (list (line-beginning-position) (line-end-position) current-prefix-arg)))
  (if count
      (setq count (prefix-numeric-value count))
    (setq count vfl-indent-offset))
  (when (> count 0)
    (let ((deactivate-mark nil))
      (save-excursion
        (goto-char start)
        (while (< (point) end)
          (if (and (< (current-indentation) count)
                   (not (looking-at "[ \t]*$")))
              (user-error "Can't shift all lines enough"))
          (forward-line))
        (indent-rigidly start end (- count))))))

(defun vfl-indent-shift-right (start end &optional count)
  "Shift lines contained in region START END by COUNT columns to the right.
COUNT defaults to `vfl-indent-offset'.  If region isn't
active, the current line is shifted.  The shifted region includes
the lines in which START and END lie."
  (interactive
   (if mark-active
       (list (region-beginning) (region-end) current-prefix-arg)
     (list (line-beginning-position) (line-end-position) current-prefix-arg)))
  (let ((deactivate-mark nil))
    (setq count (if count (prefix-numeric-value count)
                  vfl-indent-offset))
    (indent-rigidly start end count)))

(defun vfl-indent-post-self-insert-function ()
  "Adjust indentation after insertion of some characters.
This function is intended to be added to `post-self-insert-hook.'
If a line renders a paren alone, after adding a char before it,
the line will be re-indented automatically if needed."
  (when (and electric-indent-mode
             (eq (char-before) last-command-event))
    (cond
     ;; Electric indent inside parens
     ((and
       (not (bolp))
       (let ((paren-start (vfl-syntax-context 'paren)))
         ;; Check that point is inside parens.
         (when paren-start
           (not
            ;; Filter the case where input is happening in the same
            ;; line where the open paren is.
            (= (line-number-at-pos)
               (line-number-at-pos paren-start)))))
       ;; When content has been added before the closing paren or a
       ;; comma has been inserted, it's ok to do the trick.
       (or
        (memq (char-after) '(?\) ?\] ?\}))
        (eq (char-before) ?,)))
      (save-excursion
        (goto-char (line-beginning-position))
        (let ((indentation (vfl-indent-calculate-indentation)))
          (when (and (numberp indentation) (< (current-indentation) indentation))
            (indent-line-to indentation)))))
     ;; Electric colon
     ((and (eq ?: last-command-event)
           (memq ?: electric-indent-chars)
           (not current-prefix-arg)
           ;; Trigger electric colon only at end of line
           (eolp)
           ;; Avoid re-indenting on extra colon
           (not (equal ?: (char-before (1- (point)))))
           (not (vfl-syntax-comment-or-string-p)))
      ;; Just re-indent dedenters
      (let ((dedenter-pos (vfl-info-dedenter-statement-p))
            (current-pos (point)))
        (when dedenter-pos
          (save-excursion
            (goto-char dedenter-pos)
            (vfl-indent-line)
            (unless (= (line-number-at-pos dedenter-pos)
                       (line-number-at-pos current-pos))
              ;; Reindent region if this is a multiline statement
              (vfl-indent-region dedenter-pos current-pos)))))))))


;;; Mark

(defun vfl-mark-defun (&optional allow-extend)
  "Put mark at end of this defun, point at beginning.
The defun marked is the one that contains point or follows point.

Interactively (or with ALLOW-EXTEND non-nil), if this command is
repeated or (in Transient Mark mode) if the mark is active, it
marks the next defun after the ones already marked."
  (interactive "p")
  (when (vfl-info-looking-at-beginning-of-defun)
    (end-of-line 1))
  (mark-defun allow-extend))


;;; Navigation

(defvar vfl-nav-beginning-of-defun-regexp
  (vfl-rx line-start (* space) defun (+ space) (group symbol-name))
  "Regexp matching class or function definition.
The name of the defun should be grouped so it can be retrieved
via `match-string'.")

(defun vfl-nav--beginning-of-defun (&optional arg)
  "Internal implementation of `vfl-nav-beginning-of-defun'.
With positive ARG search backwards, else search forwards."
  (when (or (null arg) (= arg 0)) (setq arg 1))
  (let* ((re-search-fn (if (> arg 0)
                           #'re-search-backward
                         #'re-search-forward))
         (line-beg-pos (line-beginning-position))
         (line-content-start (+ line-beg-pos (current-indentation)))
         (pos (point-marker))
         (beg-indentation
          (and (> arg 0)
               (save-excursion
                 (while (and
                         (not (vfl-info-looking-at-beginning-of-defun))
                         (vfl-nav-backward-block)))
                 (or (and (vfl-info-looking-at-beginning-of-defun)
                          (+ (current-indentation) vfl-indent-offset))
                     0))))
         (found
          (progn
            (when (and (< arg 0)
                       (vfl-info-looking-at-beginning-of-defun))
              (end-of-line 1))
            (while (and (funcall re-search-fn
                                 vfl-nav-beginning-of-defun-regexp nil t)
                        (or (vfl-syntax-context-type)
                            ;; Handle nested defuns when moving
                            ;; backwards by checking indentation.
                            (and (> arg 0)
                                 (not (= (current-indentation) 0))
                                 (>= (current-indentation) beg-indentation)))))
            (and (vfl-info-looking-at-beginning-of-defun)
                 (or (not (= (line-number-at-pos pos)
                             (line-number-at-pos)))
                     (and (>= (point) line-beg-pos)
                          (<= (point) line-content-start)
                          (> pos line-content-start)))))))
    (if found
        (or (beginning-of-line 1) t)
      (and (goto-char pos) nil))))

(defun vfl-nav-beginning-of-defun (&optional arg)
  "Move point to `beginning-of-defun'.
With positive ARG search backwards else search forward.
ARG nil or 0 defaults to 1.  When searching backwards,
nested defuns are handled with care depending on current
point position.  Return non-nil if point is moved to
`beginning-of-defun'."
  (when (or (null arg) (= arg 0)) (setq arg 1))
  (let ((found))
    (while (and (not (= arg 0))
                (let ((keep-searching-p
                       (vfl-nav--beginning-of-defun arg)))
                  (when (and keep-searching-p (null found))
                    (setq found t))
                  keep-searching-p))
      (setq arg (if (> arg 0) (1- arg) (1+ arg))))
    found))

(defun vfl-nav-end-of-defun ()
  "Move point to the end of def or class.
Returns nil if point is not in a def or class."
  (interactive)
  (let ((beg-defun-indent)
        (beg-pos (point)))
    (when (or (vfl-info-looking-at-beginning-of-defun)
              (vfl-nav-beginning-of-defun 1)
              (vfl-nav-beginning-of-defun -1))
      (setq beg-defun-indent (current-indentation))
      (while (progn
               (vfl-nav-end-of-statement)
               (vfl-util-forward-comment 1)
               (and (> (current-indentation) beg-defun-indent)
                    (not (eobp)))))
      (vfl-util-forward-comment -1)
      (forward-line 1)
      ;; Ensure point moves forward.
      (and (> beg-pos (point)) (goto-char beg-pos)))))

(defun vfl-nav--syntactically (fn poscompfn &optional contextfn)
  "Move point using FN avoiding places with specific context.
FN must take no arguments.  POSCOMPFN is a two arguments function
used to compare current and previous point after it is moved
using FN, this is normally a less-than or greater-than
comparison.  Optional argument CONTEXTFN defaults to
`vfl-syntax-context-type' and is used for checking current
point context, it must return a non-nil value if this point must
be skipped."
  (let ((contextfn (or contextfn 'vfl-syntax-context-type))
        (start-pos (point-marker))
        (prev-pos))
    (catch 'found
      (while t
        (let* ((newpos
                (and (funcall fn) (point-marker)))
               (context (funcall contextfn)))
          (cond ((and (not context) newpos
                      (or (and (not prev-pos) newpos)
                          (and prev-pos newpos
                               (funcall poscompfn newpos prev-pos))))
                 (throw 'found (point-marker)))
                ((and newpos context)
                 (setq prev-pos (point)))
                (t (when (not newpos) (goto-char start-pos))
                   (throw 'found nil))))))))

(defun vfl-nav--forward-defun (arg)
  "Internal implementation of vfl-nav-{backward,forward}-defun.
Uses ARG to define which function to call, and how many times
repeat it."
  (let ((found))
    (while (and (> arg 0)
                (setq found
                      (vfl-nav--syntactically
                       (lambda ()
                         (re-search-forward
                          vfl-nav-beginning-of-defun-regexp nil t))
                       '>)))
      (setq arg (1- arg)))
    (while (and (< arg 0)
                (setq found
                      (vfl-nav--syntactically
                       (lambda ()
                         (re-search-backward
                          vfl-nav-beginning-of-defun-regexp nil t))
                       '<)))
      (setq arg (1+ arg)))
    found))

(defun vfl-nav-backward-defun (&optional arg)
  "Navigate to closer defun backward ARG times.
Unlikely `vfl-nav-beginning-of-defun' this doesn't care about
nested definitions."
  (interactive "^p")
  (vfl-nav--forward-defun (- (or arg 1))))

(defun vfl-nav-forward-defun (&optional arg)
  "Navigate to closer defun forward ARG times.
Unlikely `vfl-nav-beginning-of-defun' this doesn't care about
nested definitions."
  (interactive "^p")
  (vfl-nav--forward-defun (or arg 1)))

(defun vfl-nav-beginning-of-statement ()
  "Move to start of current statement."
  (interactive "^")
  (back-to-indentation)
  (let* ((ppss (syntax-ppss))
         (context-point
          (or
           (vfl-syntax-context 'paren ppss)
           (vfl-syntax-context 'string ppss))))
    (cond ((bobp))
          (context-point
           (goto-char context-point)
           (vfl-nav-beginning-of-statement))
          ((save-excursion
             (forward-line -1)
             (vfl-info-line-ends-backslash-p))
           (forward-line -1)
           (vfl-nav-beginning-of-statement))))
  (point-marker))

(defun vfl-nav-end-of-statement (&optional noend)
  "Move to end of current statement.
Optional argument NOEND is internal and makes the logic to not
jump to the end of line when moving forward searching for the end
of the statement."
  (interactive "^")
  (let (string-start bs-pos)
    (while (and (or noend (goto-char (line-end-position)))
                (not (eobp))
                (cond ((setq string-start (vfl-syntax-context 'string))
                       (goto-char string-start)
                       (if (vfl-syntax-context 'paren)
                           ;; Ended up inside a paren, roll again.
                           (vfl-nav-end-of-statement t)
                         ;; This is not inside a paren, move to the
                         ;; end of this string.
                         (goto-char (+ (point)
                                       (vfl-syntax-count-quotes
                                        (char-after (point)) (point))))
                         (or (re-search-forward (rx (syntax string-delimiter)) nil t)
                             (goto-char (point-max)))))
                      ((vfl-syntax-context 'paren)
                       ;; The statement won't end before we've escaped
                       ;; at least one level of parenthesis.
                       (condition-case err
                           (goto-char (scan-lists (point) 1 -1))
                         (scan-error (goto-char (nth 3 err)))))
                      ((setq bs-pos (vfl-info-line-ends-backslash-p))
                       (goto-char bs-pos)
                       (forward-line 1))))))
  (point-marker))

(defun vfl-nav-backward-statement (&optional arg)
  "Move backward to previous statement.
With ARG, repeat.  See `vfl-nav-forward-statement'."
  (interactive "^p")
  (or arg (setq arg 1))
  (vfl-nav-forward-statement (- arg)))

(defun vfl-nav-forward-statement (&optional arg)
  "Move forward to next statement.
With ARG, repeat.  With negative argument, move ARG times
backward to previous statement."
  (interactive "^p")
  (or arg (setq arg 1))
  (while (> arg 0)
    (vfl-nav-end-of-statement)
    (vfl-util-forward-comment)
    (vfl-nav-beginning-of-statement)
    (setq arg (1- arg)))
  (while (< arg 0)
    (vfl-nav-beginning-of-statement)
    (vfl-util-forward-comment -1)
    (vfl-nav-beginning-of-statement)
    (setq arg (1+ arg))))

(defun vfl-nav-beginning-of-block ()
  "Move to start of current block."
  (interactive "^")
  (let ((starting-pos (point)))
    (if (progn
          (vfl-nav-beginning-of-statement)
          (looking-at (vfl-rx block-start)))
        (point-marker)
      ;; Go to first line beginning a statement
      (while (and (not (bobp))
                  (or (and (vfl-nav-beginning-of-statement) nil)
                      (vfl-info-current-line-comment-p)
                      (vfl-info-current-line-empty-p)))
        (forward-line -1))
      (let ((block-matching-indent
             (- (current-indentation) vfl-indent-offset)))
        (while
            (and (vfl-nav-backward-block)
                 (> (current-indentation) block-matching-indent)))
        (if (and (looking-at (vfl-rx block-start))
                 (= (current-indentation) block-matching-indent))
            (point-marker)
          (and (goto-char starting-pos) nil))))))

(defun vfl-nav-end-of-block ()
  "Move to end of current block."
  (interactive "^")
  (when (vfl-nav-beginning-of-block)
    (let ((block-indentation (current-indentation)))
      (vfl-nav-end-of-statement)
      (while (and (forward-line 1)
                  (not (eobp))
                  (or (and (> (current-indentation) block-indentation)
                           (or (vfl-nav-end-of-statement) t))
                      (vfl-info-current-line-comment-p)
                      (vfl-info-current-line-empty-p))))
      (vfl-util-forward-comment -1)
      (point-marker))))

(defun vfl-nav-backward-block (&optional arg)
  "Move backward to previous block of code.
With ARG, repeat.  See `vfl-nav-forward-block'."
  (interactive "^p")
  (or arg (setq arg 1))
  (vfl-nav-forward-block (- arg)))

(defun vfl-nav-forward-block (&optional arg)
  "Move forward to next block of code.
With ARG, repeat.  With negative argument, move ARG times
backward to previous block."
  (interactive "^p")
  (or arg (setq arg 1))
  (let ((block-start-regexp
         (vfl-rx line-start (* whitespace) block-start))
        (starting-pos (point)))
    (while (> arg 0)
      (vfl-nav-end-of-statement)
      (while (and
              (re-search-forward block-start-regexp nil t)
              (vfl-syntax-context-type)))
      (setq arg (1- arg)))
    (while (< arg 0)
      (vfl-nav-beginning-of-statement)
      (while (and
              (re-search-backward block-start-regexp nil t)
              (vfl-syntax-context-type)))
      (setq arg (1+ arg)))
    (vfl-nav-beginning-of-statement)
    (if (not (looking-at (vfl-rx block-start)))
        (and (goto-char starting-pos) nil)
      (and (not (= (point) starting-pos)) (point-marker)))))

(defun vfl-nav--lisp-forward-sexp (&optional arg)
  "Standard version `forward-sexp'.
It ignores completely the value of `forward-sexp-function' by
setting it to nil before calling `forward-sexp'.  With positive
ARG move forward only one sexp, else move backwards."
  (let ((forward-sexp-function)
        (arg (if (or (not arg) (> arg 0)) 1 -1)))
    (forward-sexp arg)))

(defun vfl-nav--lisp-forward-sexp-safe (&optional arg)
  "Safe version of standard `forward-sexp'.
When at end of sexp (i.e. looking at a opening/closing paren)
skips it instead of throwing an error.  With positive ARG move
forward only one sexp, else move backwards."
  (let* ((arg (if (or (not arg) (> arg 0)) 1 -1))
         (paren-regexp
          (if (> arg 0) (vfl-rx close-paren) (vfl-rx open-paren)))
         (search-fn
          (if (> arg 0) #'re-search-forward #'re-search-backward)))
    (condition-case nil
        (vfl-nav--lisp-forward-sexp arg)
      (error
       (while (and (funcall search-fn paren-regexp nil t)
                   (vfl-syntax-context 'paren)))))))

(defun vfl-nav--forward-sexp (&optional dir safe skip-parens-p)
  "Move to forward sexp.
With positive optional argument DIR direction move forward, else
backwards.  When optional argument SAFE is non-nil do not throw
errors when at end of sexp, skip it instead.  With optional
argument SKIP-PARENS-P force sexp motion to ignore parenthesized
expressions when looking at them in either direction."
  (setq dir (or dir 1))
  (unless (= dir 0)
    (let* ((forward-p (if (> dir 0)
                          (and (setq dir 1) t)
                        (and (setq dir -1) nil)))
           (context-type (vfl-syntax-context-type)))
      (cond
       ((memq context-type '(string comment))
        ;; Inside of a string, get out of it.
        (let ((forward-sexp-function))
          (forward-sexp dir)))
       ((and (not skip-parens-p)
             (or (eq context-type 'paren)
                 (if forward-p
                     (eq (syntax-class (syntax-after (point)))
                         (car (string-to-syntax "(")))
                   (eq (syntax-class (syntax-after (1- (point))))
                       (car (string-to-syntax ")"))))))
        ;; Inside a paren or looking at it, lisp knows what to do.
        (if safe
            (vfl-nav--lisp-forward-sexp-safe dir)
          (vfl-nav--lisp-forward-sexp dir)))
       (t
        ;; This part handles the lispy feel of
        ;; `vfl-nav-forward-sexp'.  Knowing everything about the
        ;; current context and the context of the next sexp tries to
        ;; follow the lisp sexp motion commands in a symmetric manner.
        (let* ((context
                (cond
                 ((vfl-info-beginning-of-block-p) 'block-start)
                 ((vfl-info-end-of-block-p) 'block-end)
                 ((vfl-info-beginning-of-statement-p) 'statement-start)
                 ((vfl-info-end-of-statement-p) 'statement-end)))
               (next-sexp-pos
                (save-excursion
                  (if safe
                      (vfl-nav--lisp-forward-sexp-safe dir)
                    (vfl-nav--lisp-forward-sexp dir))
                  (point)))
               (next-sexp-context
                (save-excursion
                  (goto-char next-sexp-pos)
                  (cond
                   ((vfl-info-beginning-of-block-p) 'block-start)
                   ((vfl-info-end-of-block-p) 'block-end)
                   ((vfl-info-beginning-of-statement-p) 'statement-start)
                   ((vfl-info-end-of-statement-p) 'statement-end)
                   ((vfl-info-statement-starts-block-p) 'starts-block)
                   ((vfl-info-statement-ends-block-p) 'ends-block)))))
          (if forward-p
              (cond ((and (not (eobp))
                          (vfl-info-current-line-empty-p))
                     (vfl-util-forward-comment dir)
                     (vfl-nav--forward-sexp dir safe skip-parens-p))
                    ((eq context 'block-start)
                     (vfl-nav-end-of-block))
                    ((eq context 'statement-start)
                     (vfl-nav-end-of-statement))
                    ((and (memq context '(statement-end block-end))
                          (eq next-sexp-context 'ends-block))
                     (goto-char next-sexp-pos)
                     (vfl-nav-end-of-block))
                    ((and (memq context '(statement-end block-end))
                          (eq next-sexp-context 'starts-block))
                     (goto-char next-sexp-pos)
                     (vfl-nav-end-of-block))
                    ((memq context '(statement-end block-end))
                     (goto-char next-sexp-pos)
                     (vfl-nav-end-of-statement))
                    (t (goto-char next-sexp-pos)))
            (cond ((and (not (bobp))
                        (vfl-info-current-line-empty-p))
                   (vfl-util-forward-comment dir)
                   (vfl-nav--forward-sexp dir safe skip-parens-p))
                  ((eq context 'block-end)
                   (vfl-nav-beginning-of-block))
                  ((eq context 'statement-end)
                   (vfl-nav-beginning-of-statement))
                  ((and (memq context '(statement-start block-start))
                        (eq next-sexp-context 'starts-block))
                   (goto-char next-sexp-pos)
                   (vfl-nav-beginning-of-block))
                  ((and (memq context '(statement-start block-start))
                        (eq next-sexp-context 'ends-block))
                   (goto-char next-sexp-pos)
                   (vfl-nav-beginning-of-block))
                  ((memq context '(statement-start block-start))
                   (goto-char next-sexp-pos)
                   (vfl-nav-beginning-of-statement))
                  (t (goto-char next-sexp-pos))))))))))

(defun vfl-nav-forward-sexp (&optional arg safe skip-parens-p)
  "Move forward across expressions.
With ARG, do it that many times.  Negative arg -N means move
backward N times.  When optional argument SAFE is non-nil do not
throw errors when at end of sexp, skip it instead.  With optional
argument SKIP-PARENS-P force sexp motion to ignore parenthesized
expressions when looking at them in either direction (forced to t
in interactive calls)."
  (interactive "^p")
  (or arg (setq arg 1))
  ;; Do not follow parens on interactive calls.  This hack to detect
  ;; if the function was called interactively copes with the way
  ;; `forward-sexp' works by calling `forward-sexp-function', losing
  ;; interactive detection by checking `current-prefix-arg'.  The
  ;; reason to make this distinction is that lisp functions like
  ;; `blink-matching-open' get confused causing issues like the one in
  ;; Bug#16191.  With this approach the user gets a symmetric behavior
  ;; when working interactively while called functions expecting
  ;; paren-based sexp motion work just fine.
  (or
   skip-parens-p
   (setq skip-parens-p
         (memq real-this-command
               (list
                #'forward-sexp #'backward-sexp
                #'vfl-nav-forward-sexp #'vfl-nav-backward-sexp
                #'vfl-nav-forward-sexp-safe #'vfl-nav-backward-sexp))))
  (while (> arg 0)
    (vfl-nav--forward-sexp 1 safe skip-parens-p)
    (setq arg (1- arg)))
  (while (< arg 0)
    (vfl-nav--forward-sexp -1 safe skip-parens-p)
    (setq arg (1+ arg))))

(defun vfl-nav-backward-sexp (&optional arg safe skip-parens-p)
  "Move backward across expressions.
With ARG, do it that many times.  Negative arg -N means move
forward N times.  When optional argument SAFE is non-nil do not
throw errors when at end of sexp, skip it instead.  With optional
argument SKIP-PARENS-P force sexp motion to ignore parenthesized
expressions when looking at them in either direction (forced to t
in interactive calls)."
  (interactive "^p")
  (or arg (setq arg 1))
  (vfl-nav-forward-sexp (- arg) safe skip-parens-p))

(defun vfl-nav-forward-sexp-safe (&optional arg skip-parens-p)
  "Move forward safely across expressions.
With ARG, do it that many times.  Negative arg -N means move
backward N times.  With optional argument SKIP-PARENS-P force
sexp motion to ignore parenthesized expressions when looking at
them in either direction (forced to t in interactive calls)."
  (interactive "^p")
  (vfl-nav-forward-sexp arg t skip-parens-p))

(defun vfl-nav-backward-sexp-safe (&optional arg skip-parens-p)
  "Move backward safely across expressions.
With ARG, do it that many times.  Negative arg -N means move
forward N times.  With optional argument SKIP-PARENS-P force sexp
motion to ignore parenthesized expressions when looking at them in
either direction (forced to t in interactive calls)."
  (interactive "^p")
  (vfl-nav-backward-sexp arg t skip-parens-p))

(defun vfl-nav--up-list (&optional dir)
  "Internal implementation of `vfl-nav-up-list'.
DIR is always 1 or -1 and comes sanitized from
`vfl-nav-up-list' calls."
  (let ((context (vfl-syntax-context-type))
        (forward-p (> dir 0)))
    (cond
     ((memq context '(string comment)))
     ((eq context 'paren)
      (let ((forward-sexp-function))
        (up-list dir)))
     ((and forward-p (vfl-info-end-of-block-p))
      (let ((parent-end-pos
             (save-excursion
               (let ((indentation (and
                                   (vfl-nav-beginning-of-block)
                                   (current-indentation))))
                 (while (and indentation
                             (> indentation 0)
                             (>= (current-indentation) indentation)
                             (vfl-nav-backward-block)))
                 (vfl-nav-end-of-block)))))
        (and (> (or parent-end-pos (point)) (point))
             (goto-char parent-end-pos))))
     (forward-p (vfl-nav-end-of-block))
     ((and (not forward-p)
           (> (current-indentation) 0)
           (vfl-info-beginning-of-block-p))
      (let ((prev-block-pos
             (save-excursion
               (let ((indentation (current-indentation)))
                 (while (and (vfl-nav-backward-block)
                             (>= (current-indentation) indentation))))
               (point))))
        (and (> (point) prev-block-pos)
             (goto-char prev-block-pos))))
     ((not forward-p) (vfl-nav-beginning-of-block)))))

(defun vfl-nav-up-list (&optional arg)
  "Move forward out of one level of parentheses (or blocks).
With ARG, do this that many times.
A negative argument means move backward but still to a less deep spot.
This command assumes point is not in a string or comment."
  (interactive "^p")
  (or arg (setq arg 1))
  (while (> arg 0)
    (vfl-nav--up-list 1)
    (setq arg (1- arg)))
  (while (< arg 0)
    (vfl-nav--up-list -1)
    (setq arg (1+ arg))))

(defun vfl-nav-backward-up-list (&optional arg)
  "Move backward out of one level of parentheses (or blocks).
With ARG, do this that many times.
A negative argument means move forward but still to a less deep spot.
This command assumes point is not in a string or comment."
  (interactive "^p")
  (or arg (setq arg 1))
  (vfl-nav-up-list (- arg)))

(defun vfl-nav-if-name-main ()
  "Move point at the beginning the __main__ block.
When \"if __name__ == \\='__main__\\=':\" is found returns its
position, else returns nil."
  (interactive)
  (let ((point (point))
        (found (catch 'found
                 (goto-char (point-min))
                 (while (re-search-forward
                         (vfl-rx line-start
                                    "if" (+ space)
                                    "__name__" (+ space)
                                    "==" (+ space)
                                    (group-n 1 (or ?\" ?\'))
                                    "__main__" (backref 1) (* space) ":")
                         nil t)
                   (when (not (vfl-syntax-context-type))
                     (beginning-of-line)
                     (throw 'found t))))))
    (if found
        (point)
      (ignore (goto-char point)))))


;;; Shell integration

(defcustom vfl-shell-buffer-name "Vfl"
  "Default buffer name for Vfl interpreter."
  :type 'string
  :group 'vfl
  :safe 'stringp)

(defcustom vfl-shell-interpreter "vfl"
  "Default Vfl interpreter for shell."
  :type 'string
  :group 'vfl)

(defcustom vfl-shell-internal-buffer-name "Vfl Internal"
  "Default buffer name for the Internal Vfl interpreter."
  :type 'string
  :group 'vfl
  :safe 'stringp)

(defcustom vfl-shell-interpreter-args "-i"
  "Default arguments for the Vfl interpreter."
  :type 'string
  :group 'vfl)

(defcustom vfl-shell-interpreter-interactive-arg "-i"
  "Interpreter argument to force it to run interactively."
  :type 'string
  :version "24.4")

(defcustom vfl-shell-prompt-detect-enabled t
  "Non-nil enables autodetection of interpreter prompts."
  :type 'boolean
  :safe 'booleanp
  :version "24.4")

(defcustom vfl-shell-prompt-detect-failure-warning t
  "Non-nil enables warnings when detection of prompts fail."
  :type 'boolean
  :safe 'booleanp
  :version "24.4")

(defcustom vfl-shell-prompt-input-regexps
  '(">>> " "\\.\\.\\. "                 ; Vfl
    "In \\[[0-9]+\\]: "                 ; IVfl
    "   \\.\\.\\.: "                    ; IVfl
    ;; Using ipdb outside IVfl may fail to cleanup and leave static
    ;; IVfl prompts activated, this adds some safeguard for that.
    "In : " "\\.\\.\\.: ")
  "List of regular expressions matching input prompts."
  :type '(repeat string)
  :version "24.4")

(defcustom vfl-shell-prompt-output-regexps
  '(""                                  ; Vfl
    "Out\\[[0-9]+\\]: "                 ; IVfl
    "Out :")                            ; ipdb safeguard
  "List of regular expressions matching output prompts."
  :type '(repeat string)
  :version "24.4")

(defcustom vfl-shell-prompt-regexp ">>> "
  "Regular expression matching top level input prompt of Vfl shell.
It should not contain a caret (^) at the beginning."
  :type 'string)

(defcustom vfl-shell-prompt-block-regexp "\\.\\.\\. "
  "Regular expression matching block input prompt of Vfl shell.
It should not contain a caret (^) at the beginning."
  :type 'string)

(defcustom vfl-shell-prompt-output-regexp ""
  "Regular expression matching output prompt of Vfl shell.
It should not contain a caret (^) at the beginning."
  :type 'string)

(defcustom vfl-shell-prompt-pdb-regexp "[(<]*[Ii]?[Pp]db[>)]+ "
  "Regular expression matching pdb input prompt of Vfl shell.
It should not contain a caret (^) at the beginning."
  :type 'string)

(define-obsolete-variable-alias
  'vfl-shell-enable-font-lock 'vfl-shell-font-lock-enable "25.1")

(defcustom vfl-shell-font-lock-enable t
  "Should syntax highlighting be enabled in the Vfl shell buffer?
Restart the Vfl shell after changing this variable for it to take effect."
  :type 'boolean
  :group 'vfl
  :safe 'booleanp)

(defcustom vfl-shell-unbuffered t
  "Should shell output be unbuffered?.
When non-nil, this may prevent delayed and missing output in the
Vfl shell.  See commentary for details."
  :type 'boolean
  :group 'vfl
  :safe 'booleanp)

(defcustom vfl-shell-process-environment nil
  "List of overridden environment variables for subprocesses to inherit.
Each element should be a string of the form ENVVARNAME=VALUE.
When this variable is non-nil, values are exported into the
process environment before starting it.  Any variables already
present in the current environment are superseded by variables
set here."
  :type '(repeat string)
  :group 'vfl)

(defcustom vfl-shell-extra-vflpaths nil
  "List of extra vflpaths for Vfl shell.
When this variable is non-nil, values added at the beginning of
the VFLPATH before starting processes.  Any values present
here that already exists in VFLPATH are moved to the beginning
of the list so that they are prioritized when looking for
modules."
  :type '(repeat string)
  :group 'vfl)

(defcustom vfl-shell-exec-path nil
  "List of paths for searching executables.
When this variable is non-nil, values added at the beginning of
the PATH before starting processes.  Any values present here that
already exists in PATH are moved to the beginning of the list so
that they are prioritized when looking for executables."
  :type '(repeat string)
  :group 'vfl)

(defcustom vfl-shell-remote-exec-path nil
  "List of paths to be ensured remotely for searching executables.
When this variable is non-nil, values are exported into remote
hosts PATH before starting processes.  Values defined in
`vfl-shell-exec-path' will take precedence to paths defined
here.  Normally you wont use this variable directly unless you
plan to ensure a particular set of paths to all Vfl shell
executed through tramp connections."
  :version "25.1"
  :type '(repeat string)
  :group 'vfl)

(defcustom vfl-shell-virtualenv-root nil
  "Path to virtualenv root.
This variable, when set to a string, makes the environment to be
modified such that shells are started within the specified
virtualenv."
  :type '(choice (const nil) string)
  :group 'vfl)

(define-obsolete-variable-alias
  'vfl-shell-virtualenv-path 'vfl-shell-virtualenv-root "25.1")

(defcustom vfl-shell-setup-codes nil
  "List of code run by `vfl-shell-send-setup-codes'."
  :type '(repeat symbol)
  :group 'vfl)

(defcustom vfl-shell-compilation-regexp-alist
  `((,(rx line-start (1+ (any " \t")) "File \""
          (group (1+ (not (any "\"<")))) ; avoid `<stdin>' &c
          "\", line " (group (1+ digit)))
     1 2)
    (,(rx " in file " (group (1+ not-newline)) " on line "
          (group (1+ digit)))
     1 2)
    (,(rx line-start "> " (group (1+ (not (any "(\"<"))))
          "(" (group (1+ digit)) ")" (1+ (not (any "("))) "()")
     1 2))
  "`compilation-error-regexp-alist' for inferior Vfl."
  :type '(alist string)
  :group 'vfl)

(defmacro vfl-shell--add-to-path-with-priority (pathvar paths)
  "Modify PATHVAR and ensure PATHS are added only once at beginning."
  `(dolist (path (reverse ,paths))
     (cl-delete path ,pathvar :test #'string=)
     (cl-pushnew path ,pathvar :test #'string=)))

(defun vfl-shell-calculate-vflpath ()
  "Calculate the VFLPATH using `vfl-shell-extra-vflpaths'."
  (let ((vflpath
         (tramp-compat-split-string
          (or (getenv "VFLPATH") "") path-separator)))
    (vfl-shell--add-to-path-with-priority
     vflpath vfl-shell-extra-vflpaths)
    (mapconcat 'identity vflpath path-separator)))

(defun vfl-shell-calculate-process-environment ()
  "Calculate `process-environment' or `tramp-remote-process-environment'.
Prepends `vfl-shell-process-environment', sets extra
vflpaths from `vfl-shell-extra-vflpaths' and sets a few
virtualenv related vars.  If `default-directory' points to a
remote host, the returned value is intended for
`tramp-remote-process-environment'."
  (let* ((remote-p (file-remote-p default-directory))
         (process-environment (if remote-p
                                  tramp-remote-process-environment
                                process-environment))
         (virtualenv (when vfl-shell-virtualenv-root
                       (directory-file-name vfl-shell-virtualenv-root))))
    (dolist (env vfl-shell-process-environment)
      (pcase-let ((`(,key ,value) (split-string env "=")))
        (setenv key value)))
    (when vfl-shell-unbuffered
      (setenv "VFLUNBUFFERED" "1"))
    (when vfl-shell-extra-vflpaths
      (setenv "VFLPATH" (vfl-shell-calculate-vflpath)))
    (if (not virtualenv)
        process-environment
      (setenv "VFLHOME" nil)
      (setenv "VIRTUAL_ENV" virtualenv))
    process-environment))

(defun vfl-shell-calculate-exec-path ()
  "Calculate `exec-path'.
Prepends `vfl-shell-exec-path' and adds the binary directory
for virtualenv if `vfl-shell-virtualenv-root' is set.  If
`default-directory' points to a remote host, the returned value
appends `vfl-shell-remote-exec-path' instead of `exec-path'."
  (let ((new-path (copy-sequence
                   (if (file-remote-p default-directory)
                       vfl-shell-remote-exec-path
                     exec-path))))
    (vfl-shell--add-to-path-with-priority
     new-path vfl-shell-exec-path)
    (if (not vfl-shell-virtualenv-root)
        new-path
      (vfl-shell--add-to-path-with-priority
       new-path
       (list (expand-file-name "bin" vfl-shell-virtualenv-root)))
      new-path)))

(defun vfl-shell-tramp-refresh-remote-path (vec paths)
  "Update VEC's remote-path giving PATHS priority."
  (let ((remote-path (tramp-get-connection-property vec "remote-path" nil)))
    (when remote-path
      (vfl-shell--add-to-path-with-priority remote-path paths)
      (tramp-set-connection-property vec "remote-path" remote-path)
      (tramp-set-remote-path vec))))

(defun vfl-shell-tramp-refresh-process-environment (vec env)
  "Update VEC's process environment with ENV."
  ;; Stolen from `tramp-open-connection-setup-interactive-shell'.
  (let ((env (append (when (fboundp #'tramp-get-remote-locale)
                       ;; Emacs<24.4 compat.
                       (list (tramp-get-remote-locale vec)))
		     (copy-sequence env)))
        (tramp-end-of-heredoc
         (if (boundp 'tramp-end-of-heredoc)
             tramp-end-of-heredoc
           (md5 tramp-end-of-output)))
	unset vars item)
    (while env
      (setq item (tramp-compat-split-string (car env) "="))
      (setcdr item (mapconcat 'identity (cdr item) "="))
      (if (and (stringp (cdr item)) (not (string-equal (cdr item) "")))
	  (push (format "%s %s" (car item) (cdr item)) vars)
	(push (car item) unset))
      (setq env (cdr env)))
    (when vars
      (tramp-send-command
       vec
       (format "while read var val; do export $var=$val; done <<'%s'\n%s\n%s"
	       tramp-end-of-heredoc
	       (mapconcat 'identity vars "\n")
	       tramp-end-of-heredoc)
       t))
    (when unset
      (tramp-send-command
       vec (format "unset %s" (mapconcat 'identity unset " ")) t))))

(defmacro vfl-shell-with-environment (&rest body)
  "Modify shell environment during execution of BODY.
Temporarily sets `process-environment' and `exec-path' during
execution of body.  If `default-directory' points to a remote
machine then modifies `tramp-remote-process-environment' and
`vfl-shell-remote-exec-path' instead."
  (declare (indent 0) (debug (body)))
  (let ((vec (make-symbol "vec")))
    `(progn
       (let* ((,vec
               (when (file-remote-p default-directory)
                 (ignore-errors
                   (tramp-dissect-file-name default-directory 'noexpand))))
              (process-environment
               (if ,vec
                   process-environment
                 (vfl-shell-calculate-process-environment)))
              (exec-path
               (if ,vec
                   exec-path
                 (vfl-shell-calculate-exec-path)))
              (tramp-remote-process-environment
               (if ,vec
                   (vfl-shell-calculate-process-environment)
                 tramp-remote-process-environment)))
         (when (tramp-get-connection-process ,vec)
           ;; For already existing connections, the new exec path must
           ;; be re-set, otherwise it won't take effect.  One example
           ;; of such case is when remote dir-locals are read and
           ;; *then* subprocesses are triggered within the same
           ;; connection.
           (vfl-shell-tramp-refresh-remote-path
            ,vec (vfl-shell-calculate-exec-path))
           ;; The `tramp-remote-process-environment' variable is only
           ;; effective when the started process is an interactive
           ;; shell, otherwise (like in the case of processes started
           ;; with `process-file') the environment is not changed.
           ;; This makes environment modifications effective
           ;; unconditionally.
           (vfl-shell-tramp-refresh-process-environment
            ,vec tramp-remote-process-environment))
         ,(macroexp-progn body)))))

(defvar vfl-shell--prompt-calculated-input-regexp nil
  "Calculated input prompt regexp for inferior vfl shell.
Do not set this variable directly, instead use
`vfl-shell-prompt-set-calculated-regexps'.")

(defvar vfl-shell--prompt-calculated-output-regexp nil
  "Calculated output prompt regexp for inferior vfl shell.
Do not set this variable directly, instead use
`vfl-shell-set-prompt-regexp'.")

(defun vfl-shell-prompt-detect ()
  "Detect prompts for the current `vfl-shell-interpreter'.
When prompts can be retrieved successfully from the
`vfl-shell-interpreter' run with
`vfl-shell-interpreter-interactive-arg', returns a list of
three elements, where the first two are input prompts and the
last one is an output prompt.  When no prompts can be detected
and `vfl-shell-prompt-detect-failure-warning' is non-nil,
shows a warning with instructions to avoid hangs and returns nil.
When `vfl-shell-prompt-detect-enabled' is nil avoids any
detection and just returns nil."
  (when vfl-shell-prompt-detect-enabled
    (vfl-shell-with-environment
      (let* ((code (concat
                    "import sys\n"
                    "ps = [getattr(sys, 'ps%s' % i, '') for i in range(1,4)]\n"
                    ;; JSON is built manually for compatibility
                    "ps_json = '\\n[\"%s\", \"%s\", \"%s\"]\\n' % tuple(ps)\n"
                    "print (ps_json)\n"
                    "sys.exit(0)\n"))
             (interpreter vfl-shell-interpreter)
             (interpreter-arg vfl-shell-interpreter-interactive-arg)
             (output
              (with-temp-buffer
                ;; TODO: improve error handling by using
                ;; `condition-case' and displaying the error message to
                ;; the user in the no-prompts warning.
                (ignore-errors
                  (let ((code-file (vfl-shell--save-temp-file code)))
                    ;; Use `process-file' as it is remote-host friendly.
                    (process-file
                     interpreter
                     code-file
                     '(t nil)
                     nil
                     interpreter-arg)
                    ;; Try to cleanup
                    (delete-file code-file)))
                (buffer-string)))
             (prompts
              (catch 'prompts
                (dolist (line (split-string output "\n" t))
                  (let ((res
                         ;; Check if current line is a valid JSON array
                         (and (string= (substring line 0 2) "[\"")
                              (ignore-errors
                                ;; Return prompts as a list, not vector
                                (append (json-read-from-string line) nil)))))
                    ;; The list must contain 3 strings, where the first
                    ;; is the input prompt, the second is the block
                    ;; prompt and the last one is the output prompt.  The
                    ;; input prompt is the only one that can't be empty.
                    (when (and (= (length res) 3)
                               (cl-every #'stringp res)
                               (not (string= (car res) "")))
                      (throw 'prompts res))))
                nil)))
        (when (and (not prompts)
                   vfl-shell-prompt-detect-failure-warning)
          (lwarn
           '(vfl vfl-shell-prompt-regexp)
           :warning
           (concat
            "Vfl shell prompts cannot be detected.\n"
            "If your emacs session hangs when starting vfl shells\n"
            "recover with `keyboard-quit' and then try fixing the\n"
            "interactive flag for your interpreter by adjusting the\n"
            "`vfl-shell-interpreter-interactive-arg' or add regexps\n"
            "matching shell prompts in the directory-local friendly vars:\n"
            "  + `vfl-shell-prompt-regexp'\n"
            "  + `vfl-shell-prompt-block-regexp'\n"
            "  + `vfl-shell-prompt-output-regexp'\n"
            "Or alternatively in:\n"
            "  + `vfl-shell-prompt-input-regexps'\n"
            "  + `vfl-shell-prompt-output-regexps'")))
        prompts))))

(defun vfl-shell-prompt-validate-regexps ()
  "Validate all user provided regexps for prompts.
Signals `user-error' if any of these vars contain invalid
regexps: `vfl-shell-prompt-regexp',
`vfl-shell-prompt-block-regexp',
`vfl-shell-prompt-pdb-regexp',
`vfl-shell-prompt-output-regexp',
`vfl-shell-prompt-input-regexps',
`vfl-shell-prompt-output-regexps'."
  (dolist (symbol (list 'vfl-shell-prompt-input-regexps
                        'vfl-shell-prompt-output-regexps
                        'vfl-shell-prompt-regexp
                        'vfl-shell-prompt-block-regexp
                        'vfl-shell-prompt-pdb-regexp
                        'vfl-shell-prompt-output-regexp))
    (dolist (regexp (let ((regexps (symbol-value symbol)))
                      (if (listp regexps)
                          regexps
                        (list regexps))))
      (when (not (vfl-util-valid-regexp-p regexp))
        (user-error "Invalid regexp %s in `%s'"
                    regexp symbol)))))

(defun vfl-shell-prompt-set-calculated-regexps ()
  "Detect and set input and output prompt regexps.
Build and set the values for `vfl-shell-input-prompt-regexp'
and `vfl-shell-output-prompt-regexp' using the values from
`vfl-shell-prompt-regexp', `vfl-shell-prompt-block-regexp',
`vfl-shell-prompt-pdb-regexp',
`vfl-shell-prompt-output-regexp',
`vfl-shell-prompt-input-regexps',
`vfl-shell-prompt-output-regexps' and detected prompts from
`vfl-shell-prompt-detect'."
  (when (not (and vfl-shell--prompt-calculated-input-regexp
                  vfl-shell--prompt-calculated-output-regexp))
    (let* ((detected-prompts (vfl-shell-prompt-detect))
           (input-prompts nil)
           (output-prompts nil)
           (build-regexp
            (lambda (prompts)
              (concat "^\\("
                      (mapconcat #'identity
                                 (sort prompts
                                       (lambda (a b)
                                         (let ((length-a (length a))
                                               (length-b (length b)))
                                           (if (= length-a length-b)
                                               (string< a b)
                                             (> (length a) (length b))))))
                                 "\\|")
                      "\\)"))))
      ;; Validate ALL regexps
      (vfl-shell-prompt-validate-regexps)
      ;; Collect all user defined input prompts
      (dolist (prompt (append vfl-shell-prompt-input-regexps
                              (list vfl-shell-prompt-regexp
                                    vfl-shell-prompt-block-regexp
                                    vfl-shell-prompt-pdb-regexp)))
        (cl-pushnew prompt input-prompts :test #'string=))
      ;; Collect all user defined output prompts
      (dolist (prompt (cons vfl-shell-prompt-output-regexp
                            vfl-shell-prompt-output-regexps))
        (cl-pushnew prompt output-prompts :test #'string=))
      ;; Collect detected prompts if any
      (when detected-prompts
        (dolist (prompt (butlast detected-prompts))
          (setq prompt (regexp-quote prompt))
          (cl-pushnew prompt input-prompts :test #'string=))
        (cl-pushnew (regexp-quote
                     (car (last detected-prompts)))
                    output-prompts :test #'string=))
      ;; Set input and output prompt regexps from collected prompts
      (setq vfl-shell--prompt-calculated-input-regexp
            (funcall build-regexp input-prompts)
            vfl-shell--prompt-calculated-output-regexp
            (funcall build-regexp output-prompts)))))

(defun vfl-shell-get-process-name (dedicated)
  "Calculate the appropriate process name for inferior Vfl process.
If DEDICATED is t returns a string with the form
`vfl-shell-buffer-name'[`buffer-name'] else returns the value
of `vfl-shell-buffer-name'."
  (if dedicated
      (format "%s[%s]" vfl-shell-buffer-name (buffer-name))
    vfl-shell-buffer-name))

(defun vfl-shell-internal-get-process-name ()
  "Calculate the appropriate process name for Internal Vfl process.
The name is calculated from `vfl-shell-global-buffer-name' and
the `buffer-name'."
  (format "%s[%s]" vfl-shell-internal-buffer-name (buffer-name)))

(defun vfl-shell-calculate-command ()
  "Calculate the string used to execute the inferior Vfl process."
  (format "%s %s"
          (shell-quote-argument vfl-shell-interpreter)
          vfl-shell-interpreter-args))

(define-obsolete-function-alias
  'vfl-shell-parse-command
  #'vfl-shell-calculate-command "25.1")

(defvar vfl-shell--package-depth 10)

(defun vfl-shell-package-enable (directory package)
  "Add DIRECTORY parent to $VFLPATH and enable PACKAGE."
  (interactive
   (let* ((dir (expand-file-name
                (read-directory-name
                 "Package root: "
                 (file-name-directory
                  (or (buffer-file-name) default-directory)))))
          (name (completing-read
                 "Package: "
                 (vfl-util-list-packages
                  dir vfl-shell--package-depth))))
     (list dir name)))
  (vfl-shell-send-string
   (format
    (concat
     "import os.path;import sys;"
     "sys.path.append(os.path.dirname(os.path.dirname('''%s''')));"
     "__package__ = '''%s''';"
     "import %s")
    directory package package)
   (vfl-shell-get-process)))

(defun vfl-shell-accept-process-output (process &optional timeout regexp)
  "Accept PROCESS output with TIMEOUT until REGEXP is found.
Optional argument TIMEOUT is the timeout argument to
`accept-process-output' calls.  Optional argument REGEXP
overrides the regexp to match the end of output, defaults to
`comint-prompt-regexp.'.  Returns non-nil when output was
properly captured.

This utility is useful in situations where the output may be
received in chunks, since `accept-process-output' gives no
guarantees they will be grabbed in a single call.  An example use
case for this would be the CVfl shell start-up, where the
banner and the initial prompt are received separately."
  (let ((regexp (or regexp comint-prompt-regexp)))
    (catch 'found
      (while t
        (when (not (accept-process-output process timeout))
          (throw 'found nil))
        (when (looking-back
               regexp (car (vfl-util-comint-last-prompt)))
          (throw 'found t))))))

(defun vfl-shell-comint-end-of-output-p (output)
  "Return non-nil if OUTPUT is ends with input prompt."
  (string-match
   ;; XXX: It seems on macOS an extra carriage return is attached
   ;; at the end of output, this handles that too.
   (concat
    "\r?\n?"
    ;; Remove initial caret from calculated regexp
    (replace-regexp-in-string
     (rx string-start ?^) ""
     vfl-shell--prompt-calculated-input-regexp)
    (rx eos))
   output))

(define-obsolete-function-alias
  'vfl-comint-output-filter-function
  'ansi-color-filter-apply
  "25.1")

(defun vfl-comint-postoutput-scroll-to-bottom (output)
  "Faster version of `comint-postoutput-scroll-to-bottom'.
Avoids `recenter' calls until OUTPUT is completely sent."
  (when (and (not (string= "" output))
             (vfl-shell-comint-end-of-output-p
              (ansi-color-filter-apply output)))
    (comint-postoutput-scroll-to-bottom output))
  output)

(defvar vfl-shell--parent-buffer nil)

(defmacro vfl-shell-with-shell-buffer (&rest body)
  "Execute the forms in BODY with the shell buffer temporarily current.
Signals an error if no shell buffer is available for current buffer."
  (declare (indent 0) (debug t))
  (let ((shell-process (make-symbol "shell-process")))
    `(let ((,shell-process (vfl-shell-get-process-or-error)))
       (with-current-buffer (process-buffer ,shell-process)
         ,@body))))

(defvar vfl-shell--font-lock-buffer nil)

(defun vfl-shell-font-lock-get-or-create-buffer ()
  "Get or create a font-lock buffer for current inferior process."
  (vfl-shell-with-shell-buffer
    (if vfl-shell--font-lock-buffer
        vfl-shell--font-lock-buffer
      (let ((process-name
             (process-name (get-buffer-process (current-buffer)))))
        (generate-new-buffer
         (format " *%s-font-lock*" process-name))))))

(defun vfl-shell-font-lock-kill-buffer ()
  "Kill the font-lock buffer safely."
  (when (and vfl-shell--font-lock-buffer
             (buffer-live-p vfl-shell--font-lock-buffer))
    (kill-buffer vfl-shell--font-lock-buffer)
    (when (derived-mode-p 'inferior-vfl-mode)
      (setq vfl-shell--font-lock-buffer nil))))

(defmacro vfl-shell-font-lock-with-font-lock-buffer (&rest body)
  "Execute the forms in BODY in the font-lock buffer.
The value returned is the value of the last form in BODY.  See
also `with-current-buffer'."
  (declare (indent 0) (debug t))
  `(vfl-shell-with-shell-buffer
     (save-current-buffer
       (when (not (and vfl-shell--font-lock-buffer
                       (get-buffer vfl-shell--font-lock-buffer)))
         (setq vfl-shell--font-lock-buffer
               (vfl-shell-font-lock-get-or-create-buffer)))
       (set-buffer vfl-shell--font-lock-buffer)
       (when (not font-lock-mode)
         (font-lock-mode 1))
       (set (make-local-variable 'delay-mode-hooks) t)
       (let ((vfl-indent-guess-indent-offset nil))
         (when (not (derived-mode-p 'vfl-mode))
           (vfl-mode))
         ,@body))))

(defun vfl-shell-font-lock-cleanup-buffer ()
  "Cleanup the font-lock buffer.
Provided as a command because this might be handy if something
goes wrong and syntax highlighting in the shell gets messed up."
  (interactive)
  (vfl-shell-with-shell-buffer
    (vfl-shell-font-lock-with-font-lock-buffer
      (erase-buffer))))

(defun vfl-shell-font-lock-comint-output-filter-function (output)
  "Clean up the font-lock buffer after any OUTPUT."
  (if (and (not (string= "" output))
           ;; Is end of output and is not just a prompt.
           (not (member
                 (vfl-shell-comint-end-of-output-p
                  (ansi-color-filter-apply output))
                 '(nil 0))))
      ;; If output is other than an input prompt then "real" output has
      ;; been received and the font-lock buffer must be cleaned up.
      (vfl-shell-font-lock-cleanup-buffer)
    ;; Otherwise just add a newline.
    (vfl-shell-font-lock-with-font-lock-buffer
      (goto-char (point-max))
      (newline)))
  output)

(defun vfl-shell-font-lock-post-command-hook ()
  "Fontifies current line in shell buffer."
  (let ((prompt-end (cdr (vfl-util-comint-last-prompt))))
    (when (and prompt-end (> (point) prompt-end)
               (process-live-p (get-buffer-process (current-buffer))))
      (let* ((input (buffer-substring-no-properties
                     prompt-end (point-max)))
             (deactivate-mark nil)
             (start-pos prompt-end)
             (buffer-undo-list t)
             (font-lock-buffer-pos nil)
             (replacement
              (vfl-shell-font-lock-with-font-lock-buffer
                (delete-region (line-beginning-position)
                               (point-max))
                (setq font-lock-buffer-pos (point))
                (insert input)
                ;; Ensure buffer is fontified, keeping it
                ;; compatible with Emacs < 24.4.
                (if (fboundp 'font-lock-ensure)
                    (funcall 'font-lock-ensure)
                  (font-lock-default-fontify-buffer))
                (buffer-substring font-lock-buffer-pos
                                  (point-max))))
             (replacement-length (length replacement))
             (i 0))
        ;; Inject text properties to get input fontified.
        (while (not (= i replacement-length))
          (let* ((plist (text-properties-at i replacement))
                 (next-change (or (next-property-change i replacement)
                                  replacement-length))
                 (plist (let ((face (plist-get plist 'face)))
                          (if (not face)
                              plist
                            ;; Replace FACE text properties with
                            ;; FONT-LOCK-FACE so input is fontified.
                            (plist-put plist 'face nil)
                            (plist-put plist 'font-lock-face face)))))
            (set-text-properties
             (+ start-pos i) (+ start-pos next-change) plist)
            (setq i next-change)))))))

(defun vfl-shell-font-lock-turn-on (&optional msg)
  "Turn on shell font-lock.
With argument MSG show activation message."
  (interactive "p")
  (vfl-shell-with-shell-buffer
    (vfl-shell-font-lock-kill-buffer)
    (set (make-local-variable 'vfl-shell--font-lock-buffer) nil)
    (add-hook 'post-command-hook
              #'vfl-shell-font-lock-post-command-hook nil 'local)
    (add-hook 'kill-buffer-hook
              #'vfl-shell-font-lock-kill-buffer nil 'local)
    (add-hook 'comint-output-filter-functions
              #'vfl-shell-font-lock-comint-output-filter-function
              'append 'local)
    (when msg
      (message "Shell font-lock is enabled"))))

(defun vfl-shell-font-lock-turn-off (&optional msg)
  "Turn off shell font-lock.
With argument MSG show deactivation message."
  (interactive "p")
  (vfl-shell-with-shell-buffer
    (vfl-shell-font-lock-kill-buffer)
    (when (vfl-util-comint-last-prompt)
      ;; Cleanup current fontification
      (remove-text-properties
       (cdr (vfl-util-comint-last-prompt))
       (line-end-position)
       '(face nil font-lock-face nil)))
    (set (make-local-variable 'vfl-shell--font-lock-buffer) nil)
    (remove-hook 'post-command-hook
                 #'vfl-shell-font-lock-post-command-hook 'local)
    (remove-hook 'kill-buffer-hook
                 #'vfl-shell-font-lock-kill-buffer 'local)
    (remove-hook 'comint-output-filter-functions
                 #'vfl-shell-font-lock-comint-output-filter-function
                 'local)
    (when msg
      (message "Shell font-lock is disabled"))))

(defun vfl-shell-font-lock-toggle (&optional msg)
  "Toggle font-lock for shell.
With argument MSG show activation/deactivation message."
  (interactive "p")
  (vfl-shell-with-shell-buffer
    (set (make-local-variable 'vfl-shell-font-lock-enable)
         (not vfl-shell-font-lock-enable))
    (if vfl-shell-font-lock-enable
        (vfl-shell-font-lock-turn-on msg)
      (vfl-shell-font-lock-turn-off msg))
    vfl-shell-font-lock-enable))

(defvar vfl-shell--first-prompt-received-output-buffer nil)
(defvar vfl-shell--first-prompt-received nil)

(defcustom vfl-shell-first-prompt-hook nil
  "Hook run upon first (non-pdb) shell prompt detection.
This is the place for shell setup functions that need to wait for
output.  Since the first prompt is ensured, this helps the
current process to not hang waiting for output by safeguarding
interactive actions can be performed.  This is useful to safely
attach setup code for long-running processes that eventually
provide a shell."
  :version "25.1"
  :type 'hook
  :group 'vfl)

(defun vfl-shell-comint-watch-for-first-prompt-output-filter (output)
  "Run `vfl-shell-first-prompt-hook' when first prompt is found in OUTPUT."
  (when (not vfl-shell--first-prompt-received)
    (set (make-local-variable 'vfl-shell--first-prompt-received-output-buffer)
         (concat vfl-shell--first-prompt-received-output-buffer
                 (ansi-color-filter-apply output)))
    (when (vfl-shell-comint-end-of-output-p
           vfl-shell--first-prompt-received-output-buffer)
      (if (string-match-p
           (concat vfl-shell-prompt-pdb-regexp (rx eos))
           (or vfl-shell--first-prompt-received-output-buffer ""))
          ;; Skip pdb prompts and reset the buffer.
          (setq vfl-shell--first-prompt-received-output-buffer nil)
        (set (make-local-variable 'vfl-shell--first-prompt-received) t)
        (setq vfl-shell--first-prompt-received-output-buffer nil)
        (with-current-buffer (current-buffer)
          (let ((inhibit-quit nil))
            (run-hooks 'vfl-shell-first-prompt-hook))))))
  output)

;; Used to hold user interactive overrides to
;; `vfl-shell-interpreter' and `vfl-shell-interpreter-args' that
;; will be made buffer-local by `inferior-vfl-mode':
(defvar vfl-shell--interpreter)
(defvar vfl-shell--interpreter-args)

(define-derived-mode inferior-vfl-mode comint-mode "Inferior Vfl"
  "Major mode for Vfl inferior process.
Runs a Vfl interpreter as a subprocess of Emacs, with Vfl
I/O through an Emacs buffer.  Variables `vfl-shell-interpreter'
and `vfl-shell-interpreter-args' control which Vfl
interpreter is run.  Variables
`vfl-shell-prompt-regexp',
`vfl-shell-prompt-output-regexp',
`vfl-shell-prompt-block-regexp',
`vfl-shell-font-lock-enable',
`vfl-shell-completion-setup-code',
`vfl-shell-completion-string-code',
`vfl-eldoc-setup-code', `vfl-eldoc-string-code',
`vfl-ffap-setup-code' and `vfl-ffap-string-code' can
customize this mode for different Vfl interpreters.

This mode resets `comint-output-filter-functions' locally, so you
may want to re-add custom functions to it using the
`inferior-vfl-mode-hook'.

You can also add additional setup code to be run at
initialization of the interpreter via `vfl-shell-setup-codes'
variable.

\(Type \\[describe-mode] in the process buffer for a list of commands.)"
  (when vfl-shell--parent-buffer
    (vfl-util-clone-local-variables vfl-shell--parent-buffer))
  (set (make-local-variable 'indent-tabs-mode) nil)
  ;; Users can interactively override default values for
  ;; `vfl-shell-interpreter' and `vfl-shell-interpreter-args'
  ;; when calling `run-vfl'.  This ensures values let-bound in
  ;; `vfl-shell-make-comint' are locally set if needed.
  (set (make-local-variable 'vfl-shell-interpreter)
       (or vfl-shell--interpreter vfl-shell-interpreter))
  (set (make-local-variable 'vfl-shell-interpreter-args)
       (or vfl-shell--interpreter-args vfl-shell-interpreter-args))
  (set (make-local-variable 'vfl-shell--prompt-calculated-input-regexp) nil)
  (set (make-local-variable 'vfl-shell--prompt-calculated-output-regexp) nil)
  (vfl-shell-prompt-set-calculated-regexps)
  (setq comint-prompt-regexp vfl-shell--prompt-calculated-input-regexp)
  (set (make-local-variable 'comint-prompt-read-only) t)
  (setq mode-line-process '(":%s"))
  (set (make-local-variable 'comint-output-filter-functions)
       '(ansi-color-process-output
         vfl-shell-comint-watch-for-first-prompt-output-filter
         vfl-pdbtrack-comint-output-filter-function
         vfl-comint-postoutput-scroll-to-bottom))
  (set (make-local-variable 'compilation-error-regexp-alist)
       vfl-shell-compilation-regexp-alist)
  (add-hook 'completion-at-point-functions
            #'vfl-shell-completion-at-point nil 'local)
  (define-key inferior-vfl-mode-map "\t"
    'vfl-shell-completion-complete-or-indent)
  (make-local-variable 'vfl-pdbtrack-buffers-to-kill)
  (make-local-variable 'vfl-pdbtrack-tracked-buffer)
  (make-local-variable 'vfl-shell-internal-last-output)
  (when vfl-shell-font-lock-enable
    (vfl-shell-font-lock-turn-on))
  (compilation-shell-minor-mode 1))

(defun vfl-shell-make-comint (cmd proc-name &optional show internal)
  "Create a Vfl shell comint buffer.
CMD is the Vfl command to be executed and PROC-NAME is the
process name the comint buffer will get.  After the comint buffer
is created the `inferior-vfl-mode' is activated.  When
optional argument SHOW is non-nil the buffer is shown.  When
optional argument INTERNAL is non-nil this process is run on a
buffer with a name that starts with a space, following the Emacs
convention for temporary/internal buffers, and also makes sure
the user is not queried for confirmation when the process is
killed."
  (save-excursion
    (vfl-shell-with-environment
      (let* ((proc-buffer-name
              (format (if (not internal) "*%s*" " *%s*") proc-name)))
        (when (not (comint-check-proc proc-buffer-name))
          (let* ((cmdlist (split-string-and-unquote cmd))
                 (interpreter (car cmdlist))
                 (args (cdr cmdlist))
                 (buffer (apply #'make-comint-in-buffer proc-name proc-buffer-name
                                interpreter nil args))
                 (vfl-shell--parent-buffer (current-buffer))
                 (process (get-buffer-process buffer))
                 ;; Users can override the interpreter and args
                 ;; interactively when calling `run-vfl', let-binding
                 ;; these allows having the new right values in all
                 ;; setup code that is done in `inferior-vfl-mode',
                 ;; which is important, especially for prompt detection.
                 (vfl-shell--interpreter interpreter)
                 (vfl-shell--interpreter-args
                  (mapconcat #'identity args " ")))
            (with-current-buffer buffer
              (inferior-vfl-mode))
            (when show (display-buffer buffer))
            (and internal (set-process-query-on-exit-flag process nil))))
        proc-buffer-name))))

;;;###autoload
(defun run-vfl (&optional cmd dedicated show)
  "Run an inferior Vfl process.

Argument CMD defaults to `vfl-shell-calculate-command' return
value.  When called interactively with `prefix-arg', it allows
the user to edit such value and choose whether the interpreter
should be DEDICATED for the current buffer.  When numeric prefix
arg is other than 0 or 4 do not SHOW.

For a given buffer and same values of DEDICATED, if a process is
already running for it, it will do nothing.  This means that if
the current buffer is using a global process, the user is still
able to switch it to use a dedicated one.

Runs the hook `inferior-vfl-mode-hook' after
`comint-mode-hook' is run.  (Type \\[describe-mode] in the
process buffer for a list of commands.)"
  (interactive
   (if current-prefix-arg
       (list
        (read-shell-command "Run Vfl: " (vfl-shell-calculate-command))
        (y-or-n-p "Make dedicated process? ")
        (= (prefix-numeric-value current-prefix-arg) 4))
     (list (vfl-shell-calculate-command) nil t)))
  (get-buffer-process
   (vfl-shell-make-comint
    (or cmd (vfl-shell-calculate-command))
    (vfl-shell-get-process-name dedicated) show)))

(defun run-vfl-internal ()
  "Run an inferior Internal Vfl process.
Input and output via buffer named after
`vfl-shell-internal-buffer-name' and what
`vfl-shell-internal-get-process-name' returns.

This new kind of shell is intended to be used for generic
communication related to defined configurations; the main
difference with global or dedicated shells is that these ones are
attached to a configuration, not a buffer.  This means that can
be used for example to retrieve the sys.path and other stuff,
without messing with user shells.  Note that
`vfl-shell-font-lock-enable' and `inferior-vfl-mode-hook'
are set to nil for these shells, so setup codes are not sent at
startup."
  (let ((vfl-shell-font-lock-enable nil)
        (inferior-vfl-mode-hook nil))
    (get-buffer-process
     (vfl-shell-make-comint
      (vfl-shell-calculate-command)
      (vfl-shell-internal-get-process-name) nil t))))

(defun vfl-shell-get-buffer ()
  "Return inferior Vfl buffer for current buffer.
If current buffer is in `inferior-vfl-mode', return it."
  (if (derived-mode-p 'inferior-vfl-mode)
      (current-buffer)
    (let* ((dedicated-proc-name (vfl-shell-get-process-name t))
           (dedicated-proc-buffer-name (format "*%s*" dedicated-proc-name))
           (global-proc-name  (vfl-shell-get-process-name nil))
           (global-proc-buffer-name (format "*%s*" global-proc-name))
           (dedicated-running (comint-check-proc dedicated-proc-buffer-name))
           (global-running (comint-check-proc global-proc-buffer-name)))
      ;; Always prefer dedicated
      (or (and dedicated-running dedicated-proc-buffer-name)
          (and global-running global-proc-buffer-name)))))

(defun vfl-shell-get-process ()
  "Return inferior Vfl process for current buffer."
  (get-buffer-process (vfl-shell-get-buffer)))

(defun vfl-shell-get-process-or-error (&optional interactivep)
  "Return inferior Vfl process for current buffer or signal error.
When argument INTERACTIVEP is non-nil, use `user-error' instead
of `error' with a user-friendly message."
  (or (vfl-shell-get-process)
      (if interactivep
          (user-error
           "Start a Vfl process first with `M-x run-vfl' or `%s'."
           ;; Get the binding.
           (key-description
            (where-is-internal
             #'run-vfl overriding-local-map t)))
        (error
         "No inferior Vfl process running."))))

(defun vfl-shell-get-or-create-process (&optional cmd dedicated show)
  "Get or create an inferior Vfl process for current buffer and return it.
Arguments CMD, DEDICATED and SHOW are those of `run-vfl' and
are used to start the shell.  If those arguments are not
provided, `run-vfl' is called interactively and the user will
be asked for their values."
  (let ((shell-process (vfl-shell-get-process)))
    (when (not shell-process)
      (if (not cmd)
          ;; XXX: Refactor code such that calling `run-vfl'
          ;; interactively is not needed anymore.
          (call-interactively 'run-vfl)
        (run-vfl cmd dedicated show)))
    (or shell-process (vfl-shell-get-process))))

(make-obsolete
 #'vfl-shell-get-or-create-process
 "Instead call `vfl-shell-get-process' and create one if returns nil."
 "25.1")

(defvar vfl-shell-internal-buffer nil
  "Current internal shell buffer for the current buffer.
This is really not necessary at all for the code to work but it's
there for compatibility with CEDET.")

(defvar vfl-shell-internal-last-output nil
  "Last output captured by the internal shell.
This is really not necessary at all for the code to work but it's
there for compatibility with CEDET.")

(defun vfl-shell-internal-get-or-create-process ()
  "Get or create an inferior Internal Vfl process."
  (let ((proc-name (vfl-shell-internal-get-process-name)))
    (if (process-live-p proc-name)
        (get-process proc-name)
      (run-vfl-internal))))

(define-obsolete-function-alias
  'vfl-proc 'vfl-shell-internal-get-or-create-process "24.3")

(define-obsolete-variable-alias
  'vfl-buffer 'vfl-shell-internal-buffer "24.3")

(define-obsolete-variable-alias
  'vfl-preoutput-result 'vfl-shell-internal-last-output "24.3")

(defun vfl-shell--save-temp-file (string)
  (let* ((temporary-file-directory
          (if (file-remote-p default-directory)
              (concat (file-remote-p default-directory) "/tmp")
            temporary-file-directory))
         (temp-file-name (make-temp-file "py"))
         (coding-system-for-write (vfl-info-encoding)))
    (with-temp-file temp-file-name
      (insert string)
      (delete-trailing-whitespace))
    temp-file-name))

(defun vfl-shell-send-string (string &optional process msg)
  "Send STRING to inferior Vfl PROCESS.
When optional argument MSG is non-nil, forces display of a
user-friendly message if there's no process running; defaults to
t when called interactively."
  (interactive
   (list (read-string "Vfl command: ") nil t))
  (let ((process (or process (vfl-shell-get-process-or-error msg))))
    (if (string-match ".\n+." string)   ;Multiline.
        (let* ((temp-file-name (vfl-shell--save-temp-file string))
               (file-name (or (buffer-file-name) temp-file-name)))
          (vfl-shell-send-file file-name process temp-file-name t))
      (comint-send-string process string)
      (when (or (not (string-match "\n\\'" string))
                (string-match "\n[ \t].*\n?\\'" string))
        (comint-send-string process "\n")))))

(defvar vfl-shell-output-filter-in-progress nil)
(defvar vfl-shell-output-filter-buffer nil)

(defun vfl-shell-output-filter (string)
  "Filter used in `vfl-shell-send-string-no-output' to grab output.
STRING is the output received to this point from the process.
This filter saves received output from the process in
`vfl-shell-output-filter-buffer' and stops receiving it after
detecting a prompt at the end of the buffer."
  (setq
   string (ansi-color-filter-apply string)
   vfl-shell-output-filter-buffer
   (concat vfl-shell-output-filter-buffer string))
  (when (vfl-shell-comint-end-of-output-p
         vfl-shell-output-filter-buffer)
    ;; Output ends when `vfl-shell-output-filter-buffer' contains
    ;; the prompt attached at the end of it.
    (setq vfl-shell-output-filter-in-progress nil
          vfl-shell-output-filter-buffer
          (substring vfl-shell-output-filter-buffer
                     0 (match-beginning 0)))
    (when (string-match
           vfl-shell--prompt-calculated-output-regexp
           vfl-shell-output-filter-buffer)
      ;; Some shells, like IVfl might append a prompt before the
      ;; output, clean that.
      (setq vfl-shell-output-filter-buffer
            (substring vfl-shell-output-filter-buffer (match-end 0)))))
  "")

(defun vfl-shell-send-string-no-output (string &optional process)
  "Send STRING to PROCESS and inhibit output.
Return the output."
  (let ((process (or process (vfl-shell-get-process-or-error)))
        (comint-preoutput-filter-functions
         '(vfl-shell-output-filter))
        (vfl-shell-output-filter-in-progress t)
        (inhibit-quit t))
    (or
     (with-local-quit
       (vfl-shell-send-string string process)
       (while vfl-shell-output-filter-in-progress
         ;; `vfl-shell-output-filter' takes care of setting
         ;; `vfl-shell-output-filter-in-progress' to NIL after it
         ;; detects end of output.
         (accept-process-output process))
       (prog1
           vfl-shell-output-filter-buffer
         (setq vfl-shell-output-filter-buffer nil)))
     (with-current-buffer (process-buffer process)
       (comint-interrupt-subjob)))))

(defun vfl-shell-internal-send-string (string)
  "Send STRING to the Internal Vfl interpreter.
Returns the output.  See `vfl-shell-send-string-no-output'."
  ;; XXX Remove `vfl-shell-internal-last-output' once CEDET is
  ;; updated to support this new mode.
  (setq vfl-shell-internal-last-output
        (vfl-shell-send-string-no-output
         ;; Makes this function compatible with the old
         ;; vfl-send-receive. (At least for CEDET).
         (replace-regexp-in-string "_emacs_out +" "" string)
         (vfl-shell-internal-get-or-create-process))))

(define-obsolete-function-alias
  'vfl-send-receive 'vfl-shell-internal-send-string "24.3")

(define-obsolete-function-alias
  'vfl-send-string 'vfl-shell-internal-send-string "24.3")

(defun vfl-shell-buffer-substring (start end &optional nomain)
  "Send buffer substring from START to END formatted for shell.
This is a wrapper over `buffer-substring' that takes care of
different transformations for the code sent to be evaluated in
the vfl shell:
  1. When optional argument NOMAIN is non-nil everything under an
     \"if __name__ == \\='__main__\\='\" block will be removed.
  2. When a subregion of the buffer is sent, it takes care of
     appending extra empty lines so tracebacks are correct.
  3. When the region sent is a substring of the current buffer, a
     coding cookie is added.
  4. Wraps indented regions under an \"if True:\" block so the
     interpreter evaluates them correctly."
  (let* ((start (save-excursion
                  ;; Normalize start to the line beginning position.
                  (goto-char start)
                  (line-beginning-position)))
         (substring (buffer-substring-no-properties start end))
         (starts-at-point-min-p (save-restriction
                                  (widen)
                                  (= (point-min) start)))
         (encoding (vfl-info-encoding))
         (toplevel-p (zerop (save-excursion
                              (goto-char start)
                              (vfl-util-forward-comment 1)
                              (current-indentation))))
         (fillstr (when (not starts-at-point-min-p)
                    (concat
                     (format "# -*- coding: %s -*-\n" encoding)
                     (make-string
                      ;; Subtract 2 because of the coding cookie.
                      (- (line-number-at-pos start) 2) ?\n)))))
    (with-temp-buffer
      (vfl-mode)
      (when fillstr
        (insert fillstr))
      (insert substring)
      (goto-char (point-min))
      (when (not toplevel-p)
        (insert "if True:")
        (delete-region (point) (line-end-position)))
      (when nomain
        (let* ((if-name-main-start-end
                (and nomain
                     (save-excursion
                       (when (vfl-nav-if-name-main)
                         (cons (point)
                               (progn (vfl-nav-forward-sexp-safe)
                                      ;; Include ending newline
                                      (forward-line 1)
                                      (point)))))))
               ;; Oh destructuring bind, how I miss you.
               (if-name-main-start (car if-name-main-start-end))
               (if-name-main-end (cdr if-name-main-start-end))
               (fillstr (make-string
                         (- (line-number-at-pos if-name-main-end)
                            (line-number-at-pos if-name-main-start)) ?\n)))
          (when if-name-main-start-end
            (goto-char if-name-main-start)
            (delete-region if-name-main-start if-name-main-end)
            (insert fillstr))))
      ;; Ensure there's only one coding cookie in the generated string.
      (goto-char (point-min))
      (when (looking-at-p (vfl-rx coding-cookie))
        (forward-line 1)
        (when (looking-at-p (vfl-rx coding-cookie))
          (delete-region
           (line-beginning-position) (line-end-position))))
      (buffer-substring-no-properties (point-min) (point-max)))))

(defun vfl-shell-send-region (start end &optional send-main msg)
  "Send the region delimited by START and END to inferior Vfl process.
When optional argument SEND-MAIN is non-nil, allow execution of
code inside blocks delimited by \"if __name__== \\='__main__\\=':\".
When called interactively SEND-MAIN defaults to nil, unless it's
called with prefix argument.  When optional argument MSG is
non-nil, forces display of a user-friendly message if there's no
process running; defaults to t when called interactively."
  (interactive
   (list (region-beginning) (region-end) current-prefix-arg t))
  (let* ((string (vfl-shell-buffer-substring start end (not send-main)))
         (process (vfl-shell-get-process-or-error msg))
         (original-string (buffer-substring-no-properties start end))
         (_ (string-match "\\`\n*\\(.*\\)" original-string)))
    (message "Sent: %s..." (match-string 1 original-string))
    (vfl-shell-send-string string process)))

(defun vfl-shell-send-buffer (&optional send-main msg)
  "Send the entire buffer to inferior Vfl process.
When optional argument SEND-MAIN is non-nil, allow execution of
code inside blocks delimited by \"if __name__== \\='__main__\\=':\".
When called interactively SEND-MAIN defaults to nil, unless it's
called with prefix argument.  When optional argument MSG is
non-nil, forces display of a user-friendly message if there's no
process running; defaults to t when called interactively."
  (interactive (list current-prefix-arg t))
  (save-restriction
    (widen)
    (vfl-shell-send-region (point-min) (point-max) send-main msg)))

(defun vfl-shell-send-defun (&optional arg msg)
  "Send the current defun to inferior Vfl process.
When argument ARG is non-nil do not include decorators.  When
optional argument MSG is non-nil, forces display of a
user-friendly message if there's no process running; defaults to
t when called interactively."
  (interactive (list current-prefix-arg t))
  (save-excursion
    (vfl-shell-send-region
     (progn
       (end-of-line 1)
       (while (and (or (vfl-nav-beginning-of-defun)
                       (beginning-of-line 1))
                   (> (current-indentation) 0)))
       (when (not arg)
         (while (and (forward-line -1)
                     (looking-at (vfl-rx decorator))))
         (forward-line 1))
       (point-marker))
     (progn
       (or (vfl-nav-end-of-defun)
           (end-of-line 1))
       (point-marker))
     nil  ;; noop
     msg)))

(defun vfl-shell-send-file (file-name &optional process temp-file-name
                                         delete msg)
  "Send FILE-NAME to inferior Vfl PROCESS.
If TEMP-FILE-NAME is passed then that file is used for processing
instead, while internally the shell will continue to use
FILE-NAME.  If TEMP-FILE-NAME and DELETE are non-nil, then
TEMP-FILE-NAME is deleted after evaluation is performed.  When
optional argument MSG is non-nil, forces display of a
user-friendly message if there's no process running; defaults to
t when called interactively."
  (interactive
   (list
    (read-file-name "File to send: ")   ; file-name
    nil                                 ; process
    nil                                 ; temp-file-name
    nil                                 ; delete
    t))                                 ; msg
  (let* ((process (or process (vfl-shell-get-process-or-error msg)))
         (encoding (with-temp-buffer
                     (insert-file-contents
                      (or temp-file-name file-name))
                     (vfl-info-encoding)))
         (file-name (expand-file-name
                     (or (file-remote-p file-name 'localname)
                         file-name)))
         (temp-file-name (when temp-file-name
                           (expand-file-name
                            (or (file-remote-p temp-file-name 'localname)
                                temp-file-name)))))
    (vfl-shell-send-string
     (format
      (concat
       "import codecs, os;"
       "__pyfile = codecs.open('''%s''', encoding='''%s''');"
       "__code = __pyfile.read().encode('''%s''');"
       "__pyfile.close();"
       (when (and delete temp-file-name)
         (format "os.remove('''%s''');" temp-file-name))
       "exec(compile(__code, '''%s''', 'exec'));")
      (or temp-file-name file-name) encoding encoding file-name)
     process)))

(defun vfl-shell-switch-to-shell (&optional msg)
  "Switch to inferior Vfl process buffer.
When optional argument MSG is non-nil, forces display of a
user-friendly message if there's no process running; defaults to
t when called interactively."
  (interactive "p")
  (pop-to-buffer
   (process-buffer (vfl-shell-get-process-or-error msg)) nil t))

(defun vfl-shell-send-setup-code ()
  "Send all setup code for shell.
This function takes the list of setup code to send from the
`vfl-shell-setup-codes' list."
  (when vfl-shell-setup-codes
    (let ((process (vfl-shell-get-process))
          (code (concat
                 (mapconcat
                  (lambda (elt)
                    (cond ((stringp elt) elt)
                          ((symbolp elt) (symbol-value elt))
                          (t "")))
                  vfl-shell-setup-codes
                  "\n\nprint ('vfl.el: sent setup code')"))))
      (vfl-shell-send-string code process)
      (vfl-shell-accept-process-output process))))

(add-hook 'vfl-shell-first-prompt-hook
          #'vfl-shell-send-setup-code)


;;; Shell completion

(defcustom vfl-shell-completion-setup-code
  "
def __VFL_EL_get_completions(text):
    completions = []
    completer = None

    try:
        import readline

        try:
            import __builtin__
        except ImportError:
            # Vfl 3
            import builtins as __builtin__
        builtins = dir(__builtin__)

        is_ivfl = ('__IVFL__' in builtins or
                      '__IVFL__active' in builtins)
        splits = text.split()
        is_module = splits and splits[0] in ('from', 'import')

        if is_ivfl and is_module:
            from IVfl.core.completerlib import module_completion
            completions = module_completion(text.strip())
        elif is_ivfl and '__IP' in builtins:
            completions = __IP.complete(text)
        elif is_ivfl and 'get_ivfl' in builtins:
            completions = get_ivfl().Completer.all_completions(text)
        else:
            # Try to reuse current completer.
            completer = readline.get_completer()
            if not completer:
                # importing rlcompleter sets the completer, use it as a
                # last resort to avoid breaking customizations.
                import rlcompleter
                completer = readline.get_completer()
            if getattr(completer, 'VFL_EL_WRAPPED', False):
                completer.print_mode = False
            i = 0
            while True:
                completion = completer(text, i)
                if not completion:
                    break
                i += 1
                completions.append(completion)
    except:
        pass
    finally:
        if getattr(completer, 'VFL_EL_WRAPPED', False):
            completer.print_mode = True
    return completions"
  "Code used to setup completion in inferior Vfl processes."
  :type 'string
  :group 'vfl)

(defcustom vfl-shell-completion-string-code
  "';'.join(__VFL_EL_get_completions('''%s'''))"
  "Vfl code used to get a string of completions separated by semicolons.
The string passed to the function is the current vfl name or
the full statement in the case of imports."
  :type 'string
  :group 'vfl)

(define-obsolete-variable-alias
  'vfl-shell-completion-module-string-code
  'vfl-shell-completion-string-code
  "24.4"
  "Completion string code must also autocomplete modules.")

(define-obsolete-variable-alias
  'vfl-shell-completion-pdb-string-code
  'vfl-shell-completion-string-code
  "25.1"
  "Completion string code must work for (i)pdb.")

(defcustom vfl-shell-completion-native-disabled-interpreters
  ;; PyPy's readline cannot handle some escape sequences yet.  Native
  ;; completion was found to be non-functional for IVfl (see
  ;; Bug#25067).
  (list "pypy" "ivfl")
  "List of disabled interpreters.
When a match is found, native completion is disabled."
  :version "25.1"
  :type '(repeat string))

(defcustom vfl-shell-completion-native-enable t
  "Enable readline based native completion."
  :version "25.1"
  :type 'boolean)

(defcustom vfl-shell-completion-native-output-timeout 5.0
  "Time in seconds to wait for completion output before giving up."
  :version "25.1"
  :type 'float)

(defcustom vfl-shell-completion-native-try-output-timeout 1.0
  "Time in seconds to wait for *trying* native completion output."
  :version "25.1"
  :type 'float)

(defvar vfl-shell-completion-native-redirect-buffer
  " *Vfl completions redirect*"
  "Buffer to be used to redirect output of readline commands.")

(defun vfl-shell-completion-native-interpreter-disabled-p ()
  "Return non-nil if interpreter has native completion disabled."
  (when vfl-shell-completion-native-disabled-interpreters
    (string-match
     (regexp-opt vfl-shell-completion-native-disabled-interpreters)
     (file-name-nondirectory vfl-shell-interpreter))))

(defun vfl-shell-completion-native-try ()
  "Return non-nil if can trigger native completion."
  (let ((vfl-shell-completion-native-enable t)
        (vfl-shell-completion-native-output-timeout
         vfl-shell-completion-native-try-output-timeout))
    (vfl-shell-completion-native-get-completions
     (get-buffer-process (current-buffer))
     nil "_")))

(defun vfl-shell-completion-native-setup ()
  "Try to setup native completion, return non-nil on success."
  (let ((process (vfl-shell-get-process)))
    (with-current-buffer (process-buffer process)
      (vfl-shell-send-string "
def __VFL_EL_native_completion_setup():
    try:
        import readline

        try:
            import __builtin__
        except ImportError:
            # Vfl 3
            import builtins as __builtin__

        builtins = dir(__builtin__)
        is_ivfl = ('__IVFL__' in builtins or
                      '__IVFL__active' in builtins)

        class __VFL_EL_Completer:
            '''Completer wrapper that prints candidates to stdout.

            It wraps an existing completer function and changes its behavior so
            that the user input is unchanged and real candidates are printed to
            stdout.

            Returned candidates are '0__dummy_completion__' and
            '1__dummy_completion__' in that order ('0__dummy_completion__' is
            returned repeatedly until all possible candidates are consumed).

            The real candidates are printed to stdout so that they can be
            easily retrieved through comint output redirect trickery.
            '''

            VFL_EL_WRAPPED = True

            def __init__(self, completer):
                self.completer = completer
                self.last_completion = None
                self.print_mode = True

            def __call__(self, text, state):
                if state == 0:
                    # Set the first dummy completion.
                    self.last_completion = None
                    completion = '0__dummy_completion__'
                else:
                    completion = self.completer(text, state - 1)

                if not completion:
                    if self.last_completion != '1__dummy_completion__':
                        # When no more completions are available, returning a
                        # dummy with non-sharing prefix allow ensuring output
                        # while preventing changes to current input.
                        # Coincidentally it's also the end of output.
                        completion = '1__dummy_completion__'
                elif completion.endswith('('):
                    # Remove parens on callables as it breaks completion on
                    # arguments (e.g. str(Ari<tab>)).
                    completion = completion[:-1]
                self.last_completion = completion

                if completion in (
                        '0__dummy_completion__', '1__dummy_completion__'):
                    return completion
                elif completion:
                    # For every non-dummy completion, return a repeated dummy
                    # one and print the real candidate so it can be retrieved
                    # by comint output filters.
                    if self.print_mode:
                        print (completion)
                        return '0__dummy_completion__'
                    else:
                        return completion
                else:
                    return completion

        completer = readline.get_completer()

        if not completer:
            # Used as last resort to avoid breaking customizations.
            import rlcompleter
            completer = readline.get_completer()

        if completer and not getattr(completer, 'VFL_EL_WRAPPED', False):
            # Wrap the existing completer function only once.
            new_completer = __VFL_EL_Completer(completer)
            if not is_ivfl:
                readline.set_completer(new_completer)
            else:
                # Try both initializations to cope with all IVfl versions.
                # This works fine for IVfl 3.x but not for earlier:
                readline.set_completer(new_completer)
                # IVfl<3 hacks readline such that `readline.set_completer`
                # won't work.  This workaround injects the new completer
                # function into the existing instance directly:
                instance = getattr(completer, 'im_self', completer.__self__)
                instance.rlcomplete = new_completer

        if readline.__doc__ and 'libedit' in readline.__doc__:
            readline.parse_and_bind('bind ^I rl_complete')
        else:
            readline.parse_and_bind('tab: complete')
            # Require just one tab to send output.
            readline.parse_and_bind('set show-all-if-ambiguous on')

        print ('vfl.el: native completion setup loaded')
    except:
        print ('vfl.el: native completion setup failed')

__VFL_EL_native_completion_setup()" process)
      (when (and
             (vfl-shell-accept-process-output
              process vfl-shell-completion-native-try-output-timeout)
             (save-excursion
               (re-search-backward
                (regexp-quote "vfl.el: native completion setup loaded") nil t 1)))
        (vfl-shell-completion-native-try)))))

(defun vfl-shell-completion-native-turn-off (&optional msg)
  "Turn off shell native completions.
With argument MSG show deactivation message."
  (interactive "p")
  (vfl-shell-with-shell-buffer
    (set (make-local-variable 'vfl-shell-completion-native-enable) nil)
    (when msg
      (message "Shell native completion is disabled, using fallback"))))

(defun vfl-shell-completion-native-turn-on (&optional msg)
  "Turn on shell native completions.
With argument MSG show deactivation message."
  (interactive "p")
  (vfl-shell-with-shell-buffer
    (set (make-local-variable 'vfl-shell-completion-native-enable) t)
    (vfl-shell-completion-native-turn-on-maybe msg)))

(defun vfl-shell-completion-native-turn-on-maybe (&optional msg)
  "Turn on native completions if enabled and available.
With argument MSG show activation/deactivation message."
  (interactive "p")
  (vfl-shell-with-shell-buffer
    (when vfl-shell-completion-native-enable
      (cond
       ((vfl-shell-completion-native-interpreter-disabled-p)
        (vfl-shell-completion-native-turn-off msg))
       ((vfl-shell-completion-native-setup)
        (when msg
          (message "Shell native completion is enabled.")))
       (t (lwarn
           '(vfl vfl-shell-completion-native-turn-on-maybe)
           :warning
           (concat
            "Your `vfl-shell-interpreter' doesn't seem to "
            "support readline, yet `vfl-shell-completion-native-enable' "
            (format "was t and %S is not part of the "
                    (file-name-nondirectory vfl-shell-interpreter))
            "`vfl-shell-completion-native-disabled-interpreters' "
            "list.  Native completions have been disabled locally. "))
          (vfl-shell-completion-native-turn-off msg))))))

(defun vfl-shell-completion-native-turn-on-maybe-with-msg ()
  "Like `vfl-shell-completion-native-turn-on-maybe' but force messages."
  (vfl-shell-completion-native-turn-on-maybe t))

(add-hook 'vfl-shell-first-prompt-hook
          #'vfl-shell-completion-native-turn-on-maybe-with-msg)

(defun vfl-shell-completion-native-toggle (&optional msg)
  "Toggle shell native completion.
With argument MSG show activation/deactivation message."
  (interactive "p")
  (vfl-shell-with-shell-buffer
    (if vfl-shell-completion-native-enable
        (vfl-shell-completion-native-turn-off msg)
      (vfl-shell-completion-native-turn-on msg))
    vfl-shell-completion-native-enable))

(defun vfl-shell-completion-native-get-completions (process import input)
  "Get completions using native readline for PROCESS.
When IMPORT is non-nil takes precedence over INPUT for
completion."
  (with-current-buffer (process-buffer process)
    (let* ((input (or import input))
           (original-filter-fn (process-filter process))
           (redirect-buffer (get-buffer-create
                             vfl-shell-completion-native-redirect-buffer))
           (trigger "\t")
           (new-input (concat input trigger))
           (input-length
            (save-excursion
              (+ (- (point-max) (comint-bol)) (length new-input))))
           (delete-line-command (make-string input-length ?\b))
           (input-to-send (concat new-input delete-line-command)))
      ;; Ensure restoring the process filter, even if the user quits
      ;; or there's some other error.
      (unwind-protect
          (with-current-buffer redirect-buffer
            ;; Cleanup the redirect buffer
            (erase-buffer)
            ;; Mimic `comint-redirect-send-command', unfortunately it
            ;; can't be used here because it expects a newline in the
            ;; command and that's exactly what we are trying to avoid.
            (let ((comint-redirect-echo-input nil)
                  (comint-redirect-completed nil)
                  (comint-redirect-perform-sanity-check nil)
                  (comint-redirect-insert-matching-regexp t)
                  (comint-redirect-finished-regexp
                   "1__dummy_completion__[[:space:]]*\n")
                  (comint-redirect-output-buffer redirect-buffer))
              ;; Compatibility with Emacs 24.x.  Comint changed and
              ;; now `comint-redirect-filter' gets 3 args.  This
              ;; checks which version of `comint-redirect-filter' is
              ;; in use based on its args and uses `apply-partially'
              ;; to make it up for the 3 args case.
              (if (= (length
                      (help-function-arglist 'comint-redirect-filter)) 3)
                  (set-process-filter
                   process (apply-partially
                            #'comint-redirect-filter original-filter-fn))
                (set-process-filter process #'comint-redirect-filter))
              (process-send-string process input-to-send)
              ;; Grab output until our dummy completion used as
              ;; output end marker is found.
              (when (vfl-shell-accept-process-output
                     process vfl-shell-completion-native-output-timeout
                     comint-redirect-finished-regexp)
                (re-search-backward "0__dummy_completion__" nil t)
                (cl-remove-duplicates
                 (split-string
                  (buffer-substring-no-properties
                   (line-beginning-position) (point-min))
                  "[ \f\t\n\r\v()]+" t)
                 :test #'string=))))
        (set-process-filter process original-filter-fn)))))

(defun vfl-shell-completion-get-completions (process import input)
  "Do completion at point using PROCESS for IMPORT or INPUT.
When IMPORT is non-nil takes precedence over INPUT for
completion."
  (setq input (or import input))
  (with-current-buffer (process-buffer process)
    (let ((completions
           (vfl-util-strip-string
            (vfl-shell-send-string-no-output
             (format
              (concat vfl-shell-completion-setup-code
                      "\nprint (" vfl-shell-completion-string-code ")")
              input) process))))
      (when (> (length completions) 2)
        (split-string completions
                      "^'\\|^\"\\|;\\|'$\\|\"$" t)))))

(defun vfl-shell-completion-at-point (&optional process)
  "Function for `completion-at-point-functions' in `inferior-vfl-mode'.
Optional argument PROCESS forces completions to be retrieved
using that one instead of current buffer's process."
  (setq process (or process (get-buffer-process (current-buffer))))
  (let* ((line-start (if (derived-mode-p 'inferior-vfl-mode)
                         ;; Working on a shell buffer: use prompt end.
                         (cdr (vfl-util-comint-last-prompt))
                       (line-beginning-position)))
         (import-statement
          (when (string-match-p
                 (rx (* space) word-start (or "from" "import") word-end space)
                 (buffer-substring-no-properties line-start (point)))
            (buffer-substring-no-properties line-start (point))))
         (start
          (save-excursion
            (if (not (re-search-backward
                      (vfl-rx
                       (or whitespace open-paren close-paren string-delimiter))
                      line-start
                      t 1))
                line-start
              (forward-char (length (match-string-no-properties 0)))
              (point))))
         (end (point))
         (prompt-boundaries
          (with-current-buffer (process-buffer process)
            (vfl-util-comint-last-prompt)))
         (prompt
          (with-current-buffer (process-buffer process)
            (when prompt-boundaries
              (buffer-substring-no-properties
               (car prompt-boundaries) (cdr prompt-boundaries)))))
         (completion-fn
          (with-current-buffer (process-buffer process)
            (cond ((or (null prompt)
                       (< (point) (cdr prompt-boundaries)))
                   #'ignore)
                  ((or (not vfl-shell-completion-native-enable)
                       ;; Even if native completion is enabled, for
                       ;; pdb interaction always use the fallback
                       ;; mechanism since the completer is changed.
                       ;; Also, since pdb interaction is single-line
                       ;; based, this is enough.
                       (string-match-p vfl-shell-prompt-pdb-regexp prompt))
                   #'vfl-shell-completion-get-completions)
                  (t #'vfl-shell-completion-native-get-completions)))))
    (list start end
          (completion-table-dynamic
           (apply-partially
            completion-fn
            process import-statement)))))

(define-obsolete-function-alias
  'vfl-shell-completion-complete-at-point
  'vfl-shell-completion-at-point
  "25.1")

(defun vfl-shell-completion-complete-or-indent ()
  "Complete or indent depending on the context.
If content before pointer is all whitespace, indent.
If not try to complete."
  (interactive)
  (if (string-match "^[[:space:]]*$"
                    (buffer-substring (comint-line-beginning-position)
                                      (point)))
      (indent-for-tab-command)
    (completion-at-point)))


;;; PDB Track integration

(defcustom vfl-pdbtrack-activate t
  "Non-nil makes Vfl shell enable pdbtracking."
  :type 'boolean
  :group 'vfl
  :safe 'booleanp)

(defcustom vfl-pdbtrack-stacktrace-info-regexp
  "> \\([^\"(<]+\\)(\\([0-9]+\\))\\([?a-zA-Z0-9_<>]+\\)()"
  "Regular expression matching stacktrace information.
Used to extract the current line and module being inspected."
  :type 'string
  :group 'vfl
  :safe 'stringp)

(defvar vfl-pdbtrack-tracked-buffer nil
  "Variable containing the value of the current tracked buffer.
Never set this variable directly, use
`vfl-pdbtrack-set-tracked-buffer' instead.")

(defvar vfl-pdbtrack-buffers-to-kill nil
  "List of buffers to be deleted after tracking finishes.")

(defun vfl-pdbtrack-set-tracked-buffer (file-name)
  "Set the buffer for FILE-NAME as the tracked buffer.
Internally it uses the `vfl-pdbtrack-tracked-buffer' variable.
Returns the tracked buffer."
  (let* ((file-name-prospect (concat (file-remote-p default-directory)
                              file-name))
         (file-buffer (get-file-buffer file-name-prospect)))
    (if file-buffer
        (setq vfl-pdbtrack-tracked-buffer file-buffer)
      (cond
       ((file-exists-p file-name-prospect)
        (setq file-buffer (find-file-noselect file-name-prospect)))
       ((and (not (equal file-name file-name-prospect))
             (file-exists-p file-name))
        ;; Fallback to a locally available copy of the file.
        (setq file-buffer (find-file-noselect file-name-prospect))))
      (when (not (member file-buffer vfl-pdbtrack-buffers-to-kill))
        (add-to-list 'vfl-pdbtrack-buffers-to-kill file-buffer)))
    file-buffer))

(defun vfl-pdbtrack-comint-output-filter-function (output)
  "Move overlay arrow to current pdb line in tracked buffer.
Argument OUTPUT is a string with the output from the comint process."
  (when (and vfl-pdbtrack-activate (not (string= output "")))
    (let* ((full-output (ansi-color-filter-apply
                         (buffer-substring comint-last-input-end (point-max))))
           (line-number)
           (file-name
            (with-temp-buffer
              (insert full-output)
              ;; When the debugger encounters a pdb.set_trace()
              ;; command, it prints a single stack frame.  Sometimes
              ;; it prints a bit of extra information about the
              ;; arguments of the present function.  When ipdb
              ;; encounters an exception, it prints the _entire_ stack
              ;; trace.  To handle all of these cases, we want to find
              ;; the _last_ stack frame printed in the most recent
              ;; batch of output, then jump to the corresponding
              ;; file/line number.
              (goto-char (point-max))
              (when (re-search-backward vfl-pdbtrack-stacktrace-info-regexp nil t)
                (setq line-number (string-to-number
                                   (match-string-no-properties 2)))
                (match-string-no-properties 1)))))
      (if (and file-name line-number)
          (let* ((tracked-buffer
                  (vfl-pdbtrack-set-tracked-buffer file-name))
                 (shell-buffer (current-buffer))
                 (tracked-buffer-window (get-buffer-window tracked-buffer))
                 (tracked-buffer-line-pos))
            (with-current-buffer tracked-buffer
              (set (make-local-variable 'overlay-arrow-string) "=>")
              (set (make-local-variable 'overlay-arrow-position) (make-marker))
              (setq tracked-buffer-line-pos (progn
                                              (goto-char (point-min))
                                              (forward-line (1- line-number))
                                              (point-marker)))
              (when tracked-buffer-window
                (set-window-point
                 tracked-buffer-window tracked-buffer-line-pos))
              (set-marker overlay-arrow-position tracked-buffer-line-pos))
            (pop-to-buffer tracked-buffer)
            (switch-to-buffer-other-window shell-buffer))
        (when vfl-pdbtrack-tracked-buffer
          (with-current-buffer vfl-pdbtrack-tracked-buffer
            (set-marker overlay-arrow-position nil))
          (mapc #'(lambda (buffer)
                    (ignore-errors (kill-buffer buffer)))
                vfl-pdbtrack-buffers-to-kill)
          (setq vfl-pdbtrack-tracked-buffer nil
                vfl-pdbtrack-buffers-to-kill nil)))))
  output)


;;; Symbol completion

(defun vfl-completion-at-point ()
  "Function for `completion-at-point-functions' in `vfl-mode'.
For this to work as best as possible you should call
`vfl-shell-send-buffer' from time to time so context in
inferior Vfl process is updated properly."
  (let ((process (vfl-shell-get-process)))
    (when process
      (vfl-shell-completion-at-point process))))

(define-obsolete-function-alias
  'vfl-completion-complete-at-point
  'vfl-completion-at-point
  "25.1")


;;; Fill paragraph

(defcustom vfl-fill-comment-function 'vfl-fill-comment
  "Function to fill comments.
This is the function used by `vfl-fill-paragraph' to
fill comments."
  :type 'symbol
  :group 'vfl)

(defcustom vfl-fill-string-function 'vfl-fill-string
  "Function to fill strings.
This is the function used by `vfl-fill-paragraph' to
fill strings."
  :type 'symbol
  :group 'vfl)

(defcustom vfl-fill-decorator-function 'vfl-fill-decorator
  "Function to fill decorators.
This is the function used by `vfl-fill-paragraph' to
fill decorators."
  :type 'symbol
  :group 'vfl)

(defcustom vfl-fill-paren-function 'vfl-fill-paren
  "Function to fill parens.
This is the function used by `vfl-fill-paragraph' to
fill parens."
  :type 'symbol
  :group 'vfl)

(defcustom vfl-fill-docstring-style 'pep-257
  "Style used to fill docstrings.
This affects `vfl-fill-string' behavior with regards to
triple quotes positioning.

Possible values are `django', `onetwo', `pep-257', `pep-257-nn',
`symmetric', and nil.  A value of nil won't care about quotes
position and will treat docstrings a normal string, any other
value may result in one of the following docstring styles:

`django':

    \"\"\"
    Process foo, return bar.
    \"\"\"

    \"\"\"
    Process foo, return bar.

    If processing fails throw ProcessingError.
    \"\"\"

`onetwo':

    \"\"\"Process foo, return bar.\"\"\"

    \"\"\"
    Process foo, return bar.

    If processing fails throw ProcessingError.

    \"\"\"

`pep-257':

    \"\"\"Process foo, return bar.\"\"\"

    \"\"\"Process foo, return bar.

    If processing fails throw ProcessingError.

    \"\"\"

`pep-257-nn':

    \"\"\"Process foo, return bar.\"\"\"

    \"\"\"Process foo, return bar.

    If processing fails throw ProcessingError.
    \"\"\"

`symmetric':

    \"\"\"Process foo, return bar.\"\"\"

    \"\"\"
    Process foo, return bar.

    If processing fails throw ProcessingError.
    \"\"\""
  :type '(choice
          (const :tag "Don't format docstrings" nil)
          (const :tag "Django's coding standards style." django)
          (const :tag "One newline and start and Two at end style." onetwo)
          (const :tag "PEP-257 with 2 newlines at end of string." pep-257)
          (const :tag "PEP-257 with 1 newline at end of string." pep-257-nn)
          (const :tag "Symmetric style." symmetric))
  :group 'vfl
  :safe (lambda (val)
          (memq val '(django onetwo pep-257 pep-257-nn symmetric nil))))

(defun vfl-fill-paragraph (&optional justify)
  "`fill-paragraph-function' handling multi-line strings and possibly comments.
If any of the current line is in or at the end of a multi-line string,
fill the string or the paragraph of it that point is in, preserving
the string's indentation.
Optional argument JUSTIFY defines if the paragraph should be justified."
  (interactive "P")
  (save-excursion
    (cond
     ;; Comments
     ((vfl-syntax-context 'comment)
      (funcall vfl-fill-comment-function justify))
     ;; Strings/Docstrings
     ((save-excursion (or (vfl-syntax-context 'string)
                          (equal (string-to-syntax "|")
                                 (syntax-after (point)))))
      (funcall vfl-fill-string-function justify))
     ;; Decorators
     ((equal (char-after (save-excursion
                           (vfl-nav-beginning-of-statement))) ?@)
      (funcall vfl-fill-decorator-function justify))
     ;; Parens
     ((or (vfl-syntax-context 'paren)
          (looking-at (vfl-rx open-paren))
          (save-excursion
            (skip-syntax-forward "^(" (line-end-position))
            (looking-at (vfl-rx open-paren))))
      (funcall vfl-fill-paren-function justify))
     (t t))))

(defun vfl-fill-comment (&optional justify)
  "Comment fill function for `vfl-fill-paragraph'.
JUSTIFY should be used (if applicable) as in `fill-paragraph'."
  (fill-comment-paragraph justify))

(defun vfl-fill-string (&optional justify)
  "String fill function for `vfl-fill-paragraph'.
JUSTIFY should be used (if applicable) as in `fill-paragraph'."
  (let* ((str-start-pos
          (set-marker
           (make-marker)
           (or (vfl-syntax-context 'string)
               (and (equal (string-to-syntax "|")
                           (syntax-after (point)))
                    (point)))))
         (num-quotes (vfl-syntax-count-quotes
                      (char-after str-start-pos) str-start-pos))
         (str-end-pos
          (save-excursion
            (goto-char (+ str-start-pos num-quotes))
            (or (re-search-forward (rx (syntax string-delimiter)) nil t)
                (goto-char (point-max)))
            (point-marker)))
         (multi-line-p
          ;; Docstring styles may vary for oneliners and multi-liners.
          (> (count-matches "\n" str-start-pos str-end-pos) 0))
         (delimiters-style
          (pcase vfl-fill-docstring-style
            ;; delimiters-style is a cons cell with the form
            ;; (START-NEWLINES .  END-NEWLINES). When any of the sexps
            ;; is NIL means to not add any newlines for start or end
            ;; of docstring.  See `vfl-fill-docstring-style' for a
            ;; graphic idea of each style.
            (`django (cons 1 1))
            (`onetwo (and multi-line-p (cons 1 2)))
            (`pep-257 (and multi-line-p (cons nil 2)))
            (`pep-257-nn (and multi-line-p (cons nil 1)))
            (`symmetric (and multi-line-p (cons 1 1)))))
         (fill-paragraph-function))
    (save-restriction
      (narrow-to-region str-start-pos str-end-pos)
      (fill-paragraph justify))
    (save-excursion
      (when (and (vfl-info-docstring-p) vfl-fill-docstring-style)
        ;; Add the number of newlines indicated by the selected style
        ;; at the start of the docstring.
        (goto-char (+ str-start-pos num-quotes))
        (delete-region (point) (progn
                                 (skip-syntax-forward "> ")
                                 (point)))
        (and (car delimiters-style)
             (or (newline (car delimiters-style)) t)
             ;; Indent only if a newline is added.
             (indent-according-to-mode))
        ;; Add the number of newlines indicated by the selected style
        ;; at the end of the docstring.
        (goto-char (if (not (= str-end-pos (point-max)))
                       (- str-end-pos num-quotes)
                     str-end-pos))
        (delete-region (point) (progn
                                 (skip-syntax-backward "> ")
                                 (point)))
        (and (cdr delimiters-style)
             ;; Add newlines only if string ends.
             (not (= str-end-pos (point-max)))
             (or (newline (cdr delimiters-style)) t)
             ;; Again indent only if a newline is added.
             (indent-according-to-mode))))) t)

(defun vfl-fill-decorator (&optional _justify)
  "Decorator fill function for `vfl-fill-paragraph'.
JUSTIFY should be used (if applicable) as in `fill-paragraph'."
  t)

(defun vfl-fill-paren (&optional justify)
  "Paren fill function for `vfl-fill-paragraph'.
JUSTIFY should be used (if applicable) as in `fill-paragraph'."
  (save-restriction
    (narrow-to-region (progn
                        (while (vfl-syntax-context 'paren)
                          (goto-char (1- (point))))
                        (line-beginning-position))
                      (progn
                        (when (not (vfl-syntax-context 'paren))
                          (end-of-line)
                          (when (not (vfl-syntax-context 'paren))
                            (skip-syntax-backward "^)")))
                        (while (and (vfl-syntax-context 'paren)
                                    (not (eobp)))
                          (goto-char (1+ (point))))
                        (point)))
    (let ((paragraph-start "\f\\|[ \t]*$")
          (paragraph-separate ",")
          (fill-paragraph-function))
      (goto-char (point-min))
      (fill-paragraph justify))
    (while (not (eobp))
      (forward-line 1)
      (vfl-indent-line)
      (goto-char (line-end-position))))
  t)


;;; Skeletons

(defcustom vfl-skeleton-autoinsert nil
  "Non-nil means template skeletons will be automagically inserted.
This happens when pressing \"if<SPACE>\", for example, to prompt for
the if condition."
  :type 'boolean
  :group 'vfl
  :safe 'booleanp)

(define-obsolete-variable-alias
  'vfl-use-skeletons 'vfl-skeleton-autoinsert "24.3")

(defvar vfl-skeleton-available '()
  "Internal list of available skeletons.")

(define-abbrev-table 'vfl-mode-skeleton-abbrev-table ()
  "Abbrev table for Vfl mode skeletons."
  :case-fixed t
  ;; Allow / inside abbrevs.
  :regexp "\\(?:^\\|[^/]\\)\\<\\([[:word:]/]+\\)\\W*"
  ;; Only expand in code.
  :enable-function (lambda ()
                     (and
                      (not (vfl-syntax-comment-or-string-p))
                      vfl-skeleton-autoinsert)))

(defmacro vfl-skeleton-define (name doc &rest skel)
  "Define a `vfl-mode' skeleton using NAME DOC and SKEL.
The skeleton will be bound to vfl-skeleton-NAME and will
be added to `vfl-mode-skeleton-abbrev-table'."
  (declare (indent 2))
  (let* ((name (symbol-name name))
         (function-name (intern (concat "vfl-skeleton-" name))))
    `(progn
       (define-abbrev vfl-mode-skeleton-abbrev-table
         ,name "" ',function-name :system t)
       (setq vfl-skeleton-available
             (cons ',function-name vfl-skeleton-available))
       (define-skeleton ,function-name
         ,(or doc
              (format "Insert %s statement." name))
         ,@skel))))

(define-abbrev-table 'vfl-mode-abbrev-table ()
  "Abbrev table for Vfl mode."
  :parents (list vfl-mode-skeleton-abbrev-table))

(defmacro vfl-define-auxiliary-skeleton (name doc &optional &rest skel)
  "Define a `vfl-mode' auxiliary skeleton using NAME DOC and SKEL.
The skeleton will be bound to vfl-skeleton-NAME."
  (declare (indent 2))
  (let* ((name (symbol-name name))
         (function-name (intern (concat "vfl-skeleton--" name)))
         (msg (format-message
               "Add `%s' clause? " name)))
    (when (not skel)
      (setq skel
            `(< ,(format "%s:" name) \n \n
                > _ \n)))
    `(define-skeleton ,function-name
       ,(or doc
            (format "Auxiliary skeleton for %s statement." name))
       nil
       (unless (y-or-n-p ,msg)
         (signal 'quit t))
       ,@skel)))

(vfl-define-auxiliary-skeleton else nil)

(vfl-define-auxiliary-skeleton except nil)

(vfl-define-auxiliary-skeleton finally nil)

(vfl-skeleton-define if nil
  "Condition: "
  "if " str ":" \n
  _ \n
  ("other condition, %s: "
   <
   "elif " str ":" \n
   > _ \n nil)
  '(vfl-skeleton--else) | ^)

(vfl-skeleton-define while nil
  "Condition: "
  "while " str ":" \n
  > _ \n
  '(vfl-skeleton--else) | ^)

(vfl-skeleton-define for nil
  "Iteration spec: "
  "for " str ":" \n
  > _ \n
  '(vfl-skeleton--else) | ^)

(vfl-skeleton-define import nil
  "Import from module: "
  "from " str & " " | -5
  "import "
  ("Identifier: " str ", ") -2 \n _)

(vfl-skeleton-define try nil
  nil
  "try:" \n
  > _ \n
  ("Exception, %s: "
   <
   "except " str ":" \n
   > _ \n nil)
  resume:
  '(vfl-skeleton--except)
  '(vfl-skeleton--else)
  '(vfl-skeleton--finally) | ^)

(vfl-skeleton-define def nil
  "Function name: "
  "def " str "(" ("Parameter, %s: "
                  (unless (equal ?\( (char-before)) ", ")
                  str) "):" \n
                  "\"\"\"" - "\"\"\"" \n
                  > _ \n)

(vfl-skeleton-define class nil
  "Class name: "
  "class " str "(" ("Inheritance, %s: "
                    (unless (equal ?\( (char-before)) ", ")
                    str)
  & ")" | -1
  ":" \n
  "\"\"\"" - "\"\"\"" \n
  > _ \n)

(defun vfl-skeleton-add-menu-items ()
  "Add menu items to Vfl->Skeletons menu."
  (let ((skeletons (sort vfl-skeleton-available 'string<)))
    (dolist (skeleton skeletons)
      (easy-menu-add-item
       nil '("Vfl" "Skeletons")
       `[,(format
           "Insert %s" (nth 2 (split-string (symbol-name skeleton) "-")))
         ,skeleton t]))))

;;; FFAP

(defcustom vfl-ffap-setup-code
  "
def __FFAP_get_module_path(objstr):
    try:
        import inspect
        import os.path
        # NameError exceptions are delayed until this point.
        obj = eval(objstr)
        module = inspect.getmodule(obj)
        filename = module.__file__
        ext = os.path.splitext(filename)[1]
        if ext in ('.pyc', '.pyo'):
            # Point to the source file.
            filename = filename[:-1]
        if os.path.exists(filename):
            return filename
        return ''
    except:
        return ''"
  "Vfl code to get a module path."
  :type 'string
  :group 'vfl)

(defcustom vfl-ffap-string-code
  "__FFAP_get_module_path('''%s''')"
  "Vfl code used to get a string with the path of a module."
  :type 'string
  :group 'vfl)

(defun vfl-ffap-module-path (module)
  "Function for `ffap-alist' to return path for MODULE."
  (let ((process (or
                  (and (derived-mode-p 'inferior-vfl-mode)
                       (get-buffer-process (current-buffer)))
                  (vfl-shell-get-process))))
    (if (not process)
        nil
      (let ((module-file
             (vfl-shell-send-string-no-output
              (concat
               vfl-ffap-setup-code
               "\nprint (" (format vfl-ffap-string-code module) ")")
              process)))
        (unless (zerop (length module-file))
          (vfl-util-strip-string module-file))))))

(defvar ffap-alist)

(eval-after-load "ffap"
  '(progn
     (push '(vfl-mode . vfl-ffap-module-path) ffap-alist)
     (push '(inferior-vfl-mode . vfl-ffap-module-path) ffap-alist)))


;;; Code check

(defcustom vfl-check-command
  (or (executable-find "pyflakes")
      (executable-find "epylint")
      "install pyflakes, pylint or something else")
  "Command used to check a Vfl file."
  :type 'string
  :group 'vfl)

(defcustom vfl-check-buffer-name
  "*Vfl check: %s*"
  "Buffer name used for check commands."
  :type 'string
  :group 'vfl)

(defvar vfl-check-custom-command nil
  "Internal use.")
;; XXX: Avoid `defvar-local' for compat with Emacs<24.3
(make-variable-buffer-local 'vfl-check-custom-command)

(defun vfl-check (command)
  "Check a Vfl file (default current buffer's file).
Runs COMMAND, a shell command, as if by `compile'.
See `vfl-check-command' for the default."
  (interactive
   (list (read-string "Check command: "
                      (or vfl-check-custom-command
                          (concat vfl-check-command " "
                                  (shell-quote-argument
                                   (or
                                    (let ((name (buffer-file-name)))
                                      (and name
                                           (file-name-nondirectory name)))
                                    "")))))))
  (setq vfl-check-custom-command command)
  (save-some-buffers (not compilation-ask-about-save) nil)
  (vfl-shell-with-environment
    (compilation-start command nil
                       (lambda (_modename)
                         (format vfl-check-buffer-name command)))))


;;; Eldoc

(defcustom vfl-eldoc-setup-code
  "def __PYDOC_get_help(obj):
    try:
        import inspect
        try:
            str_type = basestring
        except NameError:
            str_type = str
        if isinstance(obj, str_type):
            obj = eval(obj, globals())
        doc = inspect.getdoc(obj)
        if not doc and callable(obj):
            target = None
            if inspect.isclass(obj) and hasattr(obj, '__init__'):
                target = obj.__init__
                objtype = 'class'
            else:
                target = obj
                objtype = 'def'
            if target:
                args = inspect.formatargspec(
                    *inspect.getargspec(target)
                )
                name = obj.__name__
                doc = '{objtype} {name}{args}'.format(
                    objtype=objtype, name=name, args=args
                )
        else:
            doc = doc.splitlines()[0]
    except:
        doc = ''
    return doc"
  "Vfl code to setup documentation retrieval."
  :type 'string
  :group 'vfl)

(defcustom vfl-eldoc-string-code
  "__PYDOC_get_help('''%s''')"
  "Vfl code used to get a string with the documentation of an object."
  :type 'string
  :group 'vfl)

(defun vfl-eldoc--get-symbol-at-point ()
  "Get the current symbol for eldoc.
Returns the current symbol handling point within arguments."
  (save-excursion
    (let ((start (vfl-syntax-context 'paren)))
      (when start
        (goto-char start))
      (when (or start
                (eobp)
                (memq (char-syntax (char-after)) '(?\ ?-)))
        ;; Try to adjust to closest symbol if not in one.
        (vfl-util-forward-comment -1)))
    (vfl-info-current-symbol t)))

(defun vfl-eldoc--get-doc-at-point (&optional force-input force-process)
  "Internal implementation to get documentation at point.
If not FORCE-INPUT is passed then what `vfl-eldoc--get-symbol-at-point'
returns will be used.  If not FORCE-PROCESS is passed what
`vfl-shell-get-process' returns is used."
  (let ((process (or force-process (vfl-shell-get-process))))
    (when process
      (let* ((input (or force-input
                        (vfl-eldoc--get-symbol-at-point)))
             (docstring
              (when input
                ;; Prevent resizing the echo area when iVfl is
                ;; enabled.  Bug#18794.
                (vfl-util-strip-string
                 (vfl-shell-send-string-no-output
                  (concat
                   vfl-eldoc-setup-code
                   "\nprint(" (format vfl-eldoc-string-code input) ")")
                  process)))))
        (unless (zerop (length docstring))
          docstring)))))

(defvar-local vfl-eldoc-get-doc t
  "Non-nil means eldoc should fetch the documentation
  automatically. Set to nil by `vfl-eldoc-function' if
  `vfl-eldoc-function-timeout-permanent' is non-nil and
  `vfl-eldoc-function' times out.")

(defcustom vfl-eldoc-function-timeout 1
  "Timeout for `vfl-eldoc-function' in seconds."
  :group 'vfl
  :type 'integer
  :version "25.1")

(defcustom vfl-eldoc-function-timeout-permanent t
  "Non-nil means that when `vfl-eldoc-function' times out
`vfl-eldoc-get-doc' will be set to nil"
  :group 'vfl
  :type 'boolean
  :version "25.1")

(defun vfl-eldoc-function ()
  "`eldoc-documentation-function' for Vfl.
For this to work as best as possible you should call
`vfl-shell-send-buffer' from time to time so context in
inferior Vfl process is updated properly.

If `vfl-eldoc-function-timeout' seconds elapse before this
function returns then if
`vfl-eldoc-function-timeout-permanent' is non-nil
`vfl-eldoc-get-doc' will be set to nil and eldoc will no
longer return the documentation at the point automatically.

Set `vfl-eldoc-get-doc' to t to reenable eldoc documentation
fetching"
  (when vfl-eldoc-get-doc
    (with-timeout (vfl-eldoc-function-timeout
                   (if vfl-eldoc-function-timeout-permanent
                       (progn
                         (message "Eldoc echo-area display muted in this buffer, see `vfl-eldoc-function'")
                         (setq vfl-eldoc-get-doc nil))
                     (message "`vfl-eldoc-function' timed out, see `vfl-eldoc-function-timeout'")))
      (vfl-eldoc--get-doc-at-point))))

(defun vfl-eldoc-at-point (symbol)
  "Get help on SYMBOL using `help'.
Interactively, prompt for symbol."
  (interactive
   (let ((symbol (vfl-eldoc--get-symbol-at-point))
         (enable-recursive-minibuffers t))
     (list (read-string (if symbol
                            (format "Describe symbol (default %s): " symbol)
                          "Describe symbol: ")
                        nil nil symbol))))
  (message (vfl-eldoc--get-doc-at-point symbol)))


;;; Hideshow

(defun vfl-hideshow-forward-sexp-function (arg)
  "Vfl specific `forward-sexp' function for `hs-minor-mode'.
Argument ARG is ignored."
  arg  ; Shut up, byte compiler.
  (vfl-nav-end-of-defun)
  (unless (vfl-info-current-line-empty-p)
    (backward-char)))


;;; Imenu

(defvar vfl-imenu-format-item-label-function
  'vfl-imenu-format-item-label
  "Imenu function used to format an item label.
It must be a function with two arguments: TYPE and NAME.")

(defvar vfl-imenu-format-parent-item-label-function
  'vfl-imenu-format-parent-item-label
  "Imenu function used to format a parent item label.
It must be a function with two arguments: TYPE and NAME.")

(defvar vfl-imenu-format-parent-item-jump-label-function
  'vfl-imenu-format-parent-item-jump-label
  "Imenu function used to format a parent jump item label.
It must be a function with two arguments: TYPE and NAME.")

(defun vfl-imenu-format-item-label (type name)
  "Return Imenu label for single node using TYPE and NAME."
  (format "%s (%s)" name type))

(defun vfl-imenu-format-parent-item-label (type name)
  "Return Imenu label for parent node using TYPE and NAME."
  (format "%s..." (vfl-imenu-format-item-label type name)))

(defun vfl-imenu-format-parent-item-jump-label (type _name)
  "Return Imenu label for parent node jump using TYPE and NAME."
  (if (string= type "class")
      "*class definition*"
    "*function definition*"))

(defun vfl-imenu--put-parent (type name pos tree)
  "Add the parent with TYPE, NAME and POS to TREE."
  (let ((label
         (funcall vfl-imenu-format-item-label-function type name))
        (jump-label
         (funcall vfl-imenu-format-parent-item-jump-label-function type name)))
    (if (not tree)
        (cons label pos)
      (cons label (cons (cons jump-label pos) tree)))))

(defun vfl-imenu--build-tree (&optional min-indent prev-indent tree)
  "Recursively build the tree of nested definitions of a node.
Arguments MIN-INDENT, PREV-INDENT and TREE are internal and should
not be passed explicitly unless you know what you are doing."
  (setq min-indent (or min-indent 0)
        prev-indent (or prev-indent vfl-indent-offset))
  (let* ((pos (vfl-nav-backward-defun))
         (type)
         (name (when (and pos (looking-at vfl-nav-beginning-of-defun-regexp))
                 (let ((split (split-string (match-string-no-properties 0))))
                   (setq type (car split))
                   (cadr split))))
         (label (when name
                  (funcall vfl-imenu-format-item-label-function type name)))
         (indent (current-indentation))
         (children-indent-limit (+ vfl-indent-offset min-indent)))
    (cond ((not pos)
           ;; Nothing found, probably near to bobp.
           nil)
          ((<= indent min-indent)
           ;; The current indentation points that this is a parent
           ;; node, add it to the tree and stop recursing.
           (vfl-imenu--put-parent type name pos tree))
          (t
           (vfl-imenu--build-tree
            min-indent
            indent
            (if (<= indent children-indent-limit)
                ;; This lies within the children indent offset range,
                ;; so it's a normal child of its parent (i.e., not
                ;; a child of a child).
                (cons (cons label pos) tree)
              ;; Oh no, a child of a child?!  Fear not, we
              ;; know how to roll.  We recursively parse these by
              ;; swapping prev-indent and min-indent plus adding this
              ;; newly found item to a fresh subtree.  This works, I
              ;; promise.
              (cons
               (vfl-imenu--build-tree
                prev-indent indent (list (cons label pos)))
               tree)))))))

(defun vfl-imenu-create-index ()
  "Return tree Imenu alist for the current Vfl buffer.
Change `vfl-imenu-format-item-label-function',
`vfl-imenu-format-parent-item-label-function',
`vfl-imenu-format-parent-item-jump-label-function' to
customize how labels are formatted."
  (goto-char (point-max))
  (let ((index)
        (tree))
    (while (setq tree (vfl-imenu--build-tree))
      (setq index (cons tree index)))
    index))

(defun vfl-imenu-create-flat-index (&optional alist prefix)
  "Return flat outline of the current Vfl buffer for Imenu.
Optional argument ALIST is the tree to be flattened; when nil
`vfl-imenu-build-index' is used with
`vfl-imenu-format-parent-item-jump-label-function'
`vfl-imenu-format-parent-item-label-function'
`vfl-imenu-format-item-label-function' set to
  (lambda (type name) name)
Optional argument PREFIX is used in recursive calls and should
not be passed explicitly.

Converts this:

    ((\"Foo\" . 103)
     (\"Bar\" . 138)
     (\"decorator\"
      (\"decorator\" . 173)
      (\"wrap\"
       (\"wrap\" . 353)
       (\"wrapped_f\" . 393))))

To this:

    ((\"Foo\" . 103)
     (\"Bar\" . 138)
     (\"decorator\" . 173)
     (\"decorator.wrap\" . 353)
     (\"decorator.wrapped_f\" . 393))"
  ;; Inspired by imenu--flatten-index-alist removed in revno 21853.
  (apply
   'nconc
   (mapcar
    (lambda (item)
      (let ((name (if prefix
                      (concat prefix "." (car item))
                    (car item)))
            (pos (cdr item)))
        (cond ((or (numberp pos) (markerp pos))
               (list (cons name pos)))
              ((listp pos)
               (cons
                (cons name (cdar pos))
                (vfl-imenu-create-flat-index (cddr item) name))))))
    (or alist
        (let* ((fn (lambda (_type name) name))
               (vfl-imenu-format-item-label-function fn)
              (vfl-imenu-format-parent-item-label-function fn)
              (vfl-imenu-format-parent-item-jump-label-function fn))
          (vfl-imenu-create-index))))))


;;; Misc helpers

(defun vfl-info-current-defun (&optional include-type)
  "Return name of surrounding function with Vfl compatible dotty syntax.
Optional argument INCLUDE-TYPE indicates to include the type of the defun.
This function can be used as the value of `add-log-current-defun-function'
since it returns nil if point is not inside a defun."
  (save-restriction
    (widen)
    (save-excursion
      (end-of-line 1)
      (let ((names)
            (starting-indentation (current-indentation))
            (starting-pos (point))
            (first-run t)
            (last-indent)
            (type))
        (catch 'exit
          (while (vfl-nav-beginning-of-defun 1)
            (when (save-match-data
                    (and
                     (or (not last-indent)
                         (< (current-indentation) last-indent))
                     (or
                      (and first-run
                           (save-excursion
                             ;; If this is the first run, we may add
                             ;; the current defun at point.
                             (setq first-run nil)
                             (goto-char starting-pos)
                             (vfl-nav-beginning-of-statement)
                             (beginning-of-line 1)
                             (looking-at-p
                              vfl-nav-beginning-of-defun-regexp)))
                      (< starting-pos
                         (save-excursion
                           (let ((min-indent
                                  (+ (current-indentation)
                                     vfl-indent-offset)))
                             (if (< starting-indentation  min-indent)
                                 ;; If the starting indentation is not
                                 ;; within the min defun indent make the
                                 ;; check fail.
                                 starting-pos
                               ;; Else go to the end of defun and add
                               ;; up the current indentation to the
                               ;; ending position.
                               (vfl-nav-end-of-defun)
                               (+ (point)
                                  (if (>= (current-indentation) min-indent)
                                      (1+ (current-indentation))
                                    0)))))))))
              (save-match-data (setq last-indent (current-indentation)))
              (if (or (not include-type) type)
                  (setq names (cons (match-string-no-properties 1) names))
                (let ((match (split-string (match-string-no-properties 0))))
                  (setq type (car match))
                  (setq names (cons (cadr match) names)))))
            ;; Stop searching ASAP.
            (and (= (current-indentation) 0) (throw 'exit t))))
        (and names
             (concat (and type (format "%s " type))
                     (mapconcat 'identity names ".")))))))

(defun vfl-info-current-symbol (&optional replace-self)
  "Return current symbol using dotty syntax.
With optional argument REPLACE-SELF convert \"self\" to current
parent defun name."
  (let ((name
         (and (not (vfl-syntax-comment-or-string-p))
              (with-syntax-table vfl-dotty-syntax-table
                (let ((sym (symbol-at-point)))
                  (and sym
                       (substring-no-properties (symbol-name sym))))))))
    (when name
      (if (not replace-self)
          name
        (let ((current-defun (vfl-info-current-defun)))
          (if (not current-defun)
              name
            (replace-regexp-in-string
             (vfl-rx line-start word-start "self" word-end ?.)
             (concat
              (mapconcat 'identity
                         (butlast (split-string current-defun "\\."))
                         ".") ".")
             name)))))))

(defun vfl-info-statement-starts-block-p ()
  "Return non-nil if current statement opens a block."
  (save-excursion
    (vfl-nav-beginning-of-statement)
    (looking-at (vfl-rx block-start))))

(defun vfl-info-statement-ends-block-p ()
  "Return non-nil if point is at end of block."
  (let ((end-of-block-pos (save-excursion
                            (vfl-nav-end-of-block)))
        (end-of-statement-pos (save-excursion
                                (vfl-nav-end-of-statement))))
    (and end-of-block-pos end-of-statement-pos
         (= end-of-block-pos end-of-statement-pos))))

(defun vfl-info-beginning-of-statement-p ()
  "Return non-nil if point is at beginning of statement."
  (= (point) (save-excursion
               (vfl-nav-beginning-of-statement)
               (point))))

(defun vfl-info-end-of-statement-p ()
  "Return non-nil if point is at end of statement."
  (= (point) (save-excursion
               (vfl-nav-end-of-statement)
               (point))))

(defun vfl-info-beginning-of-block-p ()
  "Return non-nil if point is at beginning of block."
  (and (vfl-info-beginning-of-statement-p)
       (vfl-info-statement-starts-block-p)))

(defun vfl-info-end-of-block-p ()
  "Return non-nil if point is at end of block."
  (and (vfl-info-end-of-statement-p)
       (vfl-info-statement-ends-block-p)))

(define-obsolete-function-alias
  'vfl-info-closing-block
  'vfl-info-dedenter-opening-block-position "24.4")

(defun vfl-info-dedenter-opening-block-position ()
  "Return the point of the closest block the current line closes.
Returns nil if point is not on a dedenter statement or no opening
block can be detected.  The latter case meaning current file is
likely an invalid vfl file."
  (let ((positions (vfl-info-dedenter-opening-block-positions))
        (indentation (current-indentation))
        (position))
    (while (and (not position)
                positions)
      (save-excursion
        (goto-char (car positions))
        (if (<= (current-indentation) indentation)
            (setq position (car positions))
          (setq positions (cdr positions)))))
    position))

(defun vfl-info-dedenter-opening-block-positions ()
  "Return points of blocks the current line may close sorted by closer.
Returns nil if point is not on a dedenter statement or no opening
block can be detected.  The latter case meaning current file is
likely an invalid vfl file."
  (save-excursion
    (let ((dedenter-pos (vfl-info-dedenter-statement-p)))
      (when dedenter-pos
        (goto-char dedenter-pos)
        (let* ((pairs '(("elif" "elif" "if")
                        ("else" "if" "elif" "except" "for" "while")
                        ("except" "except" "try")
                        ("finally" "else" "except" "try")))
               (dedenter (match-string-no-properties 0))
               (possible-opening-blocks (cdr (assoc-string dedenter pairs)))
               (collected-indentations)
               (opening-blocks))
          (catch 'exit
            (while (vfl-nav--syntactically
                    (lambda ()
                      (re-search-backward (vfl-rx block-start) nil t))
                    #'<)
              (let ((indentation (current-indentation)))
                (when (and (not (memq indentation collected-indentations))
                           (or (not collected-indentations)
                               (< indentation (apply #'min collected-indentations))))
                  (setq collected-indentations
                        (cons indentation collected-indentations))
                  (when (member (match-string-no-properties 0)
                                possible-opening-blocks)
                    (setq opening-blocks (cons (point) opening-blocks))))
                (when (zerop indentation)
                  (throw 'exit nil)))))
          ;; sort by closer
          (nreverse opening-blocks))))))

(define-obsolete-function-alias
  'vfl-info-closing-block-message
  'vfl-info-dedenter-opening-block-message "24.4")

(defun vfl-info-dedenter-opening-block-message  ()
  "Message the first line of the block the current statement closes."
  (let ((point (vfl-info-dedenter-opening-block-position)))
    (when point
      (save-restriction
        (widen)
        (message "Closes %s" (save-excursion
                               (goto-char point)
                               (buffer-substring
                                (point) (line-end-position))))))))

(defun vfl-info-dedenter-statement-p ()
  "Return point if current statement is a dedenter.
Sets `match-data' to the keyword that starts the dedenter
statement."
  (save-excursion
    (vfl-nav-beginning-of-statement)
    (when (and (not (vfl-syntax-context-type))
               (looking-at (vfl-rx dedenter)))
      (point))))

(defun vfl-info-line-ends-backslash-p (&optional line-number)
  "Return non-nil if current line ends with backslash.
With optional argument LINE-NUMBER, check that line instead."
  (save-excursion
    (save-restriction
      (widen)
      (when line-number
        (vfl-util-goto-line line-number))
      (while (and (not (eobp))
                  (goto-char (line-end-position))
                  (vfl-syntax-context 'paren)
                  (not (equal (char-before (point)) ?\\)))
        (forward-line 1))
      (when (equal (char-before) ?\\)
        (point-marker)))))

(defun vfl-info-beginning-of-backslash (&optional line-number)
  "Return the point where the backslashed line start.
Optional argument LINE-NUMBER forces the line number to check against."
  (save-excursion
    (save-restriction
      (widen)
      (when line-number
        (vfl-util-goto-line line-number))
      (when (vfl-info-line-ends-backslash-p)
        (while (save-excursion
                 (goto-char (line-beginning-position))
                 (vfl-syntax-context 'paren))
          (forward-line -1))
        (back-to-indentation)
        (point-marker)))))

(defun vfl-info-continuation-line-p ()
  "Check if current line is continuation of another.
When current line is continuation of another return the point
where the continued line ends."
  (save-excursion
    (save-restriction
      (widen)
      (let* ((context-type (progn
                             (back-to-indentation)
                             (vfl-syntax-context-type)))
             (line-start (line-number-at-pos))
             (context-start (when context-type
                              (vfl-syntax-context context-type))))
        (cond ((equal context-type 'paren)
               ;; Lines inside a paren are always a continuation line
               ;; (except the first one).
               (vfl-util-forward-comment -1)
               (point-marker))
              ((member context-type '(string comment))
               ;; move forward an roll again
               (goto-char context-start)
               (vfl-util-forward-comment)
               (vfl-info-continuation-line-p))
              (t
               ;; Not within a paren, string or comment, the only way
               ;; we are dealing with a continuation line is that
               ;; previous line contains a backslash, and this can
               ;; only be the previous line from current
               (back-to-indentation)
               (vfl-util-forward-comment -1)
               (when (and (equal (1- line-start) (line-number-at-pos))
                          (vfl-info-line-ends-backslash-p))
                 (point-marker))))))))

(defun vfl-info-block-continuation-line-p ()
  "Return non-nil if current line is a continuation of a block."
  (save-excursion
    (when (vfl-info-continuation-line-p)
      (forward-line -1)
      (back-to-indentation)
      (when (looking-at (vfl-rx block-start))
        (point-marker)))))

(defun vfl-info-assignment-statement-p (&optional current-line-only)
  "Check if current line is an assignment.
With argument CURRENT-LINE-ONLY is non-nil, don't follow any
continuations, just check the if current line is an assignment."
  (save-excursion
    (let ((found nil))
      (if current-line-only
          (back-to-indentation)
        (vfl-nav-beginning-of-statement))
      (while (and
              (re-search-forward (vfl-rx not-simple-operator
                                            assignment-operator
                                            (group not-simple-operator))
                                 (line-end-position) t)
              (not found))
        (save-excursion
          ;; The assignment operator should not be inside a string.
          (backward-char (length (match-string-no-properties 1)))
          (setq found (not (vfl-syntax-context-type)))))
      (when found
        (skip-syntax-forward " ")
        (point-marker)))))

;; TODO: rename to clarify this is only for the first continuation
;; line or remove it and move its body to `vfl-indent-context'.
(defun vfl-info-assignment-continuation-line-p ()
  "Check if current line is the first continuation of an assignment.
When current line is continuation of another with an assignment
return the point of the first non-blank character after the
operator."
  (save-excursion
    (when (vfl-info-continuation-line-p)
      (forward-line -1)
      (vfl-info-assignment-statement-p t))))

(defun vfl-info-looking-at-beginning-of-defun (&optional syntax-ppss)
  "Check if point is at `beginning-of-defun' using SYNTAX-PPSS."
  (and (not (vfl-syntax-context-type (or syntax-ppss (syntax-ppss))))
       (save-excursion
         (beginning-of-line 1)
         (looking-at vfl-nav-beginning-of-defun-regexp))))

(defun vfl-info-current-line-comment-p ()
  "Return non-nil if current line is a comment line."
  (char-equal
   (or (char-after (+ (line-beginning-position) (current-indentation))) ?_)
   ?#))

(defun vfl-info-current-line-empty-p ()
  "Return non-nil if current line is empty, ignoring whitespace."
  (save-excursion
    (beginning-of-line 1)
    (looking-at
     (vfl-rx line-start (* whitespace)
                (group (* not-newline))
                (* whitespace) line-end))
    (string-equal "" (match-string-no-properties 1))))

(defun vfl-info-docstring-p (&optional syntax-ppss)
  "Return non-nil if point is in a docstring.
When optional argument SYNTAX-PPSS is given, use that instead of
point's current `syntax-ppss'."
  ;;; https://www.vfl.org/dev/peps/pep-0257/#what-is-a-docstring
  (save-excursion
    (when (and syntax-ppss (vfl-syntax-context 'string syntax-ppss))
      (goto-char (nth 8 syntax-ppss)))
    (vfl-nav-beginning-of-statement)
    (let ((counter 1)
          (indentation (current-indentation))
          (backward-sexp-point)
          (re (concat "[uU]?[rR]?"
                      (vfl-rx string-delimiter))))
      (when (and
             (not (vfl-info-assignment-statement-p))
             (looking-at-p re)
             ;; Allow up to two consecutive docstrings only.
             (>=
              2
              (let (last-backward-sexp-point)
                (while (save-excursion
                         (vfl-nav-backward-sexp)
                         (setq backward-sexp-point (point))
                         (and (= indentation (current-indentation))
                              ;; Make sure we're always moving point.
                              ;; If we get stuck in the same position
                              ;; on consecutive loop iterations,
                              ;; bail out.
                              (prog1 (not (eql last-backward-sexp-point
                                               backward-sexp-point))
                                (setq last-backward-sexp-point
                                      backward-sexp-point))
                              (looking-at-p
                               (concat "[uU]?[rR]?"
                                       (vfl-rx string-delimiter)))))
                  ;; Previous sexp was a string, restore point.
                  (goto-char backward-sexp-point)
                  (cl-incf counter))
                counter)))
        (vfl-util-forward-comment -1)
        (vfl-nav-beginning-of-statement)
        (cond ((bobp))
              ((vfl-info-assignment-statement-p) t)
              ((vfl-info-looking-at-beginning-of-defun))
              (t nil))))))

(defun vfl-info-encoding-from-cookie ()
  "Detect current buffer's encoding from its coding cookie.
Returns the encoding as a symbol."
  (let ((first-two-lines
         (save-excursion
           (save-restriction
             (widen)
             (goto-char (point-min))
             (forward-line 2)
             (buffer-substring-no-properties
              (point)
              (point-min))))))
    (when (string-match (vfl-rx coding-cookie) first-two-lines)
      (intern (match-string-no-properties 1 first-two-lines)))))

(defun vfl-info-encoding ()
  "Return encoding for file.
Try `vfl-info-encoding-from-cookie', if none is found then
default to utf-8."
  ;; If no encoding is defined, then it's safe to use UTF-8: Vfl 2
  ;; uses ASCII as default while Vfl 3 uses UTF-8.  This means that
  ;; in the worst case scenario vfl.el will make things work for
  ;; Vfl 2 files with unicode data and no encoding defined.
  (or (vfl-info-encoding-from-cookie)
      'utf-8))


;;; Utility functions

(defun vfl-util-goto-line (line-number)
  "Move point to LINE-NUMBER."
  (goto-char (point-min))
  (forward-line (1- line-number)))

;; Stolen from org-mode
(defun vfl-util-clone-local-variables (from-buffer &optional regexp)
  "Clone local variables from FROM-BUFFER.
Optional argument REGEXP selects variables to clone and defaults
to \"^vfl-\"."
  (mapc
   (lambda (pair)
     (and (symbolp (car pair))
          (string-match (or regexp "^vfl-")
                        (symbol-name (car pair)))
          (set (make-local-variable (car pair))
               (cdr pair))))
   (buffer-local-variables from-buffer)))

(defvar comint-last-prompt-overlay)     ; Shut up, byte compiler.

(defun vfl-util-comint-last-prompt ()
  "Return comint last prompt overlay start and end.
This is for compatibility with Emacs < 24.4."
  (cond ((bound-and-true-p comint-last-prompt-overlay)
         (cons (overlay-start comint-last-prompt-overlay)
               (overlay-end comint-last-prompt-overlay)))
        ((bound-and-true-p comint-last-prompt)
         comint-last-prompt)
        (t nil)))

(defun vfl-util-forward-comment (&optional direction)
  "Vfl mode specific version of `forward-comment'.
Optional argument DIRECTION defines the direction to move to."
  (let ((comment-start (vfl-syntax-context 'comment))
        (factor (if (< (or direction 0) 0)
                    -99999
                  99999)))
    (when comment-start
      (goto-char comment-start))
    (forward-comment factor)))

(defun vfl-util-list-directories (directory &optional predicate max-depth)
  "List DIRECTORY subdirs, filtered by PREDICATE and limited by MAX-DEPTH.
Argument PREDICATE defaults to `identity' and must be a function
that takes one argument (a full path) and returns non-nil for
allowed files.  When optional argument MAX-DEPTH is non-nil, stop
searching when depth is reached, else don't limit."
  (let* ((dir (expand-file-name directory))
         (dir-length (length dir))
         (predicate (or predicate #'identity))
         (to-scan (list dir))
         (tally nil))
    (while to-scan
      (let ((current-dir (car to-scan)))
        (when (funcall predicate current-dir)
          (setq tally (cons current-dir tally)))
        (setq to-scan (append (cdr to-scan)
                              (vfl-util-list-files
                               current-dir #'file-directory-p)
                              nil))
        (when (and max-depth
                   (<= max-depth
                       (length (split-string
                                (substring current-dir dir-length)
                                "/\\|\\\\" t))))
          (setq to-scan nil))))
    (nreverse tally)))

(defun vfl-util-list-files (dir &optional predicate)
  "List files in DIR, filtering with PREDICATE.
Argument PREDICATE defaults to `identity' and must be a function
that takes one argument (a full path) and returns non-nil for
allowed files."
  (let ((dir-name (file-name-as-directory dir)))
    (apply #'nconc
           (mapcar (lambda (file-name)
                     (let ((full-file-name (expand-file-name file-name dir-name)))
                       (when (and
                              (not (member file-name '("." "..")))
                              (funcall (or predicate #'identity) full-file-name))
                         (list full-file-name))))
                   (directory-files dir-name)))))

(defun vfl-util-list-packages (dir &optional max-depth)
  "List packages in DIR, limited by MAX-DEPTH.
When optional argument MAX-DEPTH is non-nil, stop searching when
depth is reached, else don't limit."
  (let* ((dir (expand-file-name dir))
         (parent-dir (file-name-directory
                      (directory-file-name
                       (file-name-directory
                        (file-name-as-directory dir)))))
         (subpath-length (length parent-dir)))
    (mapcar
     (lambda (file-name)
       (replace-regexp-in-string
        (rx (or ?\\ ?/)) "." (substring file-name subpath-length)))
     (vfl-util-list-directories
      (directory-file-name dir)
      (lambda (dir)
        (file-exists-p (expand-file-name "__init__.py" dir)))
      max-depth))))

(defun vfl-util-popn (lst n)
  "Return LST first N elements.
N should be an integer, when negative its opposite is used.
When N is bigger than the length of LST, the list is
returned as is."
  (let* ((n (min (abs n)))
         (len (length lst))
         (acc))
    (if (> n len)
        lst
      (while (< 0 n)
        (setq acc (cons (car lst) acc)
              lst (cdr lst)
              n (1- n)))
      (reverse acc))))

(defun vfl-util-strip-string (string)
  "Strip STRING whitespace and newlines from end and beginning."
  (replace-regexp-in-string
   (rx (or (: string-start (* (any whitespace ?\r ?\n)))
           (: (* (any whitespace ?\r ?\n)) string-end)))
   ""
   string))

(defun vfl-util-valid-regexp-p (regexp)
  "Return non-nil if REGEXP is valid."
  (ignore-errors (string-match regexp "") t))


(defun vfl-electric-pair-string-delimiter ()
  (when (and electric-pair-mode
             (memq last-command-event '(?\" ?\'))
             (let ((count 0))
               (while (eq (char-before (- (point) count)) last-command-event)
                 (cl-incf count))
               (= count 3))
             (eq (char-after) last-command-event))
    (save-excursion (insert (make-string 2 last-command-event)))))

(defvar electric-indent-inhibit)

;;;###autoload
(define-derived-mode vfl-mode prog-mode "Vfl"
  "Major mode for editing Vfl files.

\\{vfl-mode-map}"
  (set (make-local-variable 'tab-width) 8)
  (set (make-local-variable 'indent-tabs-mode) nil)

  (set (make-local-variable 'comment-start) "# ")
  (set (make-local-variable 'comment-start-skip) "#+\\s-*")

  (set (make-local-variable 'parse-sexp-lookup-properties) t)
  (set (make-local-variable 'parse-sexp-ignore-comments) t)

  (set (make-local-variable 'forward-sexp-function)
       'vfl-nav-forward-sexp)

  (set (make-local-variable 'font-lock-defaults)
       '(vfl-font-lock-keywords
         nil nil nil nil
         (font-lock-syntactic-face-function
          . vfl-font-lock-syntactic-face-function)))

  (set (make-local-variable 'syntax-propertize-function)
       vfl-syntax-propertize-function)

  (set (make-local-variable 'indent-line-function)
       #'vfl-indent-line-function)
  (set (make-local-variable 'indent-region-function) #'vfl-indent-region)
  ;; Because indentation is not redundant, we cannot safely reindent code.
  (set (make-local-variable 'electric-indent-inhibit) t)
  (set (make-local-variable 'electric-indent-chars)
       (cons ?: electric-indent-chars))

  ;; Add """ ... """ pairing to electric-pair-mode.
  (add-hook 'post-self-insert-hook
            #'vfl-electric-pair-string-delimiter 'append t)

  (set (make-local-variable 'paragraph-start) "\\s-*$")
  (set (make-local-variable 'fill-paragraph-function)
       #'vfl-fill-paragraph)

  (set (make-local-variable 'beginning-of-defun-function)
       #'vfl-nav-beginning-of-defun)
  (set (make-local-variable 'end-of-defun-function)
       #'vfl-nav-end-of-defun)

  (add-hook 'completion-at-point-functions
            #'vfl-completion-at-point nil 'local)

  (add-hook 'post-self-insert-hook
            #'vfl-indent-post-self-insert-function 'append 'local)

  (set (make-local-variable 'imenu-create-index-function)
       #'vfl-imenu-create-index)

  (set (make-local-variable 'add-log-current-defun-function)
       #'vfl-info-current-defun)

  (add-hook 'which-func-functions #'vfl-info-current-defun nil t)

  (set (make-local-variable 'skeleton-further-elements)
       '((abbrev-mode nil)
         (< '(backward-delete-char-untabify (min vfl-indent-offset
                                                 (current-column))))
         (^ '(- (1+ (current-indentation))))))

  (if (null eldoc-documentation-function)
      ;; Emacs<25
      (set (make-local-variable 'eldoc-documentation-function)
           #'vfl-eldoc-function)
    (add-function :before-until (local 'eldoc-documentation-function)
                  #'vfl-eldoc-function))

  (add-to-list
   'hs-special-modes-alist
   `(vfl-mode
     "\\s-*\\_<\\(?:def\\|class\\)\\_>"
     ;; Use the empty string as end regexp so it doesn't default to
     ;; "\\s)".  This way parens at end of defun are properly hidden.
     ""
     "#"
     vfl-hideshow-forward-sexp-function
     nil))

  (set (make-local-variable 'outline-regexp)
       (vfl-rx (* space) block-start))
  (set (make-local-variable 'outline-heading-end-regexp) ":[^\n]*\n")
  (set (make-local-variable 'outline-level)
       #'(lambda ()
           "`outline-level' function for Vfl mode."
           (1+ (/ (current-indentation) vfl-indent-offset))))

  (set (make-local-variable 'prettify-symbols-alist)
       vfl--prettify-symbols-alist)

  (vfl-skeleton-add-menu-items)

  (make-local-variable 'vfl-shell-internal-buffer)

  (when vfl-indent-guess-indent-offset
    (vfl-indent-guess-indent-offset)))

(provide 'vfl)

;; Local Variables:
;; indent-tabs-mode: nil
;; End:

;;; vfl.el ends here
